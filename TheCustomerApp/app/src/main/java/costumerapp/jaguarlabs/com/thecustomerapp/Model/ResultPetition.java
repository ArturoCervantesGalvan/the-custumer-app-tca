package costumerapp.jaguarlabs.com.thecustomerapp.Model;

import org.json.JSONObject;

/**
 * Created by Arturo on 12/01/2016.
 */
public class ResultPetition {
    JSONObject jsonObject;
    int CodePetition;
    String AppName;
    JSONObject jsonObject2ndPetition;

    public JSONObject getJsonObject2ndPetition() {
        return jsonObject2ndPetition;
    }

    public void setJsonObject2ndPetition(JSONObject jsonObject2ndPetition) {
        this.jsonObject2ndPetition = jsonObject2ndPetition;
    }

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    //Objet use of response of petition the Service Web
    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public int getCodePetition() {
        return CodePetition;
    }

    public void setCodePetition(int codePetition) {
        CodePetition = codePetition;
    }
}
