package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

import com.google.gson.annotations.SerializedName;

import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 12/02/2016.
 */
public class DetailsProjectRequest extends BaseTokenRequest {
    @SerializedName(Util.idProject)
    private String idProject;

    public DetailsProjectRequest (String request, String idProject, String tokenUser, String refreshTokenUser){
        this.request = request;
        this.idProject = idProject;
        this.tokenUserLogin = tokenUser;
        this.refreshTokenUserLogin = refreshTokenUser;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

}
