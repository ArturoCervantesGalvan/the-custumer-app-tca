package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

import com.google.gson.annotations.SerializedName;

import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.LoginFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.User;

/**
 * Created by Arturo on 09/02/2016.
 */
public class UserResponse extends BaseResponse {
    @SerializedName(LoginFragment.user)
    public User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
