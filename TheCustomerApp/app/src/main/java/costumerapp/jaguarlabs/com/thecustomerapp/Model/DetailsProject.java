package costumerapp.jaguarlabs.com.thecustomerapp.Model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.DetailsProjectFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 13/01/2016.
 */
public class DetailsProject {
    @SerializedName(DetailsProjectFragment.comentsDetailsProject)
    String ComentsProject;
    @SerializedName(Util.idProject)
    String IdProject;
    @SerializedName(Util.nameProject)
    String NameProject;
    @SerializedName(Util.startProjectDate)
    String StartProject;
    @SerializedName(Util.endProjectDate)
    String EndProject;
    @SerializedName(Util.imageProject)
    String ImgProject;
    @SerializedName(DetailsProjectFragment.scrumMasterContact)
    ScrumMaster scrumMaster;
    @SerializedName(Util.technologyApp)
    ArrayList<Technology> technologies;

    public DetailsProject(){}

    //Constructor of the Objet
    public DetailsProject(JSONObject jsonObject) throws JSONException {
        Util.Message("Comienza crear Objeto");
        this.ComentsProject = jsonObject.getString("comentarios");
        this.IdProject = jsonObject.getString("id_proyecto");
        this.NameProject = jsonObject.getString("nombre");
        this.StartProject = Util.FormatDate(jsonObject.getString("fecha_inicio"));
        this.EndProject = Util.FormatDate(jsonObject.getString("fecha_finalizacion"));
        this.ImgProject = jsonObject.getString("imagen");
        Util.Message("Primitivos hechos");
        //Create objet of the contact
        this.scrumMaster = new ScrumMaster(jsonObject.getJSONObject("contacto"));
        Util.Message("Contacto hecho");
        Util.Message("Se creo el Objeto");
    }
    //getter and setter
    public ScrumMaster getScrumMaster() {
        return scrumMaster;
    }

    public void setScrumMaster(ScrumMaster scrumMaster) {
        this.scrumMaster = scrumMaster;
    }
    public String getComentsProject() {
        return ComentsProject;
    }

    public void setComentsProject(String comentsProject) {
        ComentsProject = comentsProject;
    }

    public String getIdProject() {
        return IdProject;
    }

    public void setIdProject(String idProject) {
        IdProject = idProject;
    }

    public String getNameProject() {
        return NameProject;
    }

    public void setNameProject(String nameProject) {
        NameProject = nameProject;
    }

    public String getStartProject() {
        return StartProject;
    }

    public void setStartProject(String startProject) {
        StartProject = startProject;
    }

    public String getEndProject() {
        return EndProject;
    }

    public void setEndProject(String endProject) {
        EndProject = endProject;
    }

    public String getImgProject() {
        return ImgProject;
    }

    public void setImgProject(String imgProject) {
        ImgProject = imgProject;
    }


}
