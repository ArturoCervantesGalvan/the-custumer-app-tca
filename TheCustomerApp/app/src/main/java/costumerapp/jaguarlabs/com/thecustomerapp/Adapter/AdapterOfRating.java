package costumerapp.jaguarlabs.com.thecustomerapp.Adapter;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.QualifyFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.MainActivity;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.OfRating;
import costumerapp.jaguarlabs.com.thecustomerapp.R;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 18/01/2016.
 */
public class AdapterOfRating extends BaseExpandableListAdapter {
    //init Object
    MainActivity mainActivity;
    ArrayList<String> TechnologyOfRating;
    ArrayList<ArrayList<OfRating>> ListOfRatings;
    LayoutInflater inflater;
    ArrayList<OfRating> ListOfRating;
    boolean inAction;
    QualifyFragment qualifyFragment;
    //Constructor
    public AdapterOfRating(MainActivity mainActivity,QualifyFragment qualifyFragment , ArrayList<String> TechnologyOfRating, ArrayList<ArrayList<OfRating>> ListOfRating, boolean Action){
        this.mainActivity = mainActivity;
        this.TechnologyOfRating = TechnologyOfRating;
        this.ListOfRatings = ListOfRating;
        this.inAction = Action;
        this.qualifyFragment = qualifyFragment;
    }
    //inflater
    public void setInflater(LayoutInflater inflater){
        this.inflater = inflater;
    }
    //function of the adapter
    @Override
    public int getGroupCount() {
        return TechnologyOfRating.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return ListOfRatings.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
    //View of the parents
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = View.inflate(mainActivity,R.layout.row_title_technology_qualify,null);
        }
        CheckedTextView checkedTextView = (CheckedTextView) convertView.findViewById(R.id.ctvTitle);
        Util.Message(" "+TechnologyOfRating.get(groupPosition)+" ");
        checkedTextView.setText(TechnologyOfRating.get(groupPosition));
        checkedTextView.setChecked(isExpanded);

        if(checkedTextView.isChecked())
            checkedTextView.setCheckMarkDrawable(R.drawable.icon_flecha_down);
        else
            checkedTextView.setCheckMarkDrawable(R.drawable.icon_flecha_up);
        return convertView;
    }
    //View of the every Sprint
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        //Set of List of the Sprint
        ListOfRating = ListOfRatings.get(groupPosition);
        //init Elements
        TextView AppName = null;
        LinearLayout llQualifyConfirm;
        final RatingBar rbQualify;
        TextView tvEmptySprint;
        //Validation of the Child is Empty
        if(ListOfRating.get(childPosition).getIdOfRating()!=null) {
            if (convertView == null) {
                convertView = View.inflate(mainActivity, R.layout.row_qualify_action, null);
            }
            AppName = (TextView) convertView.findViewById(R.id.tvSprintToQualify);
            AppName.setText(PresentSprint(ListOfRating.get(childPosition).getSprintOfRating()));
            llQualifyConfirm = (LinearLayout) convertView.findViewById(R.id.llConfirmQualify);
            rbQualify = (RatingBar) convertView.findViewById(R.id.rbQualify);
            rbQualify.setStepSize((float) 1);
            rbQualify.setNumStars(5);
            //Show Sprint in list of Qualify
            if (inAction) {
                AppName.setGravity(Gravity.CENTER_HORIZONTAL);
                llQualifyConfirm.setVisibility(View.VISIBLE);
                rbQualify.setRating(ListOfRating.get(childPosition).getQualifyOfSprint());
                //init fuction of the Qualify
                llQualifyConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Util.Message(" " + ListOfRating.get(childPosition).getTechnologyOfRating() + " " + ListOfRating.get(childPosition).getIdOfRating());
                        //Send Data of the Main Activity
                        //mainActivity.ActionQualify(ListOfRating.get(childPosition).getIdOfRating(), (int) rbQualify.getRating());
                        //((ListenerAdapterOfRating)mainActivity).ActionQualify(ListOfRating.get(childPosition).getIdOfRating(),(int) rbQualify.getRating());
                        //Set Qualify
                        ListOfRating.get(childPosition).setQualifyOfSprint((int) rbQualify.getRating());
                        //qualifyFragment.AddSprintQualify(ListOfRating.get(childPosition));
                        //Send data of the Qualify Sprint for update List of Sprint
                        ((ListenerAdapterOfRatingForQualifyFrg) qualifyFragment).AddSprintQualify(ListOfRating.get(childPosition));
                    }
                });
            } else {
                //Show Sprint in History
                llQualifyConfirm.setVisibility(View.GONE);
                AppName.setGravity(Gravity.LEFT);
                rbQualify.setRating(ListOfRating.get(childPosition).getQualifyOfSprint());
                rbQualify.isIndicator();
                rbQualify.setIsIndicator(true);
                rbQualify.isEnabled();
            }
            //Hiden and show Elements Extra
            AppName.setVisibility(View.VISIBLE);
            rbQualify.setVisibility(View.VISIBLE);
            tvEmptySprint = (TextView) convertView.findViewById(R.id.tvEmpySprint);
            tvEmptySprint.setVisibility(View.GONE);
        }else{
            //Show Child Empty
            if (convertView == null) {
                convertView = View.inflate(mainActivity, R.layout.row_qualify_action, null);
            }
            //Hide Elements and show
            AppName = (TextView) convertView.findViewById(R.id.tvSprintToQualify);
            AppName.setVisibility(View.GONE);
            llQualifyConfirm = (LinearLayout) convertView.findViewById(R.id.llConfirmQualify);
            llQualifyConfirm.setVisibility(View.GONE);
            rbQualify = (RatingBar) convertView.findViewById(R.id.rbQualify);
            rbQualify.setVisibility(View.GONE);
            tvEmptySprint = (TextView) convertView.findViewById(R.id.tvEmpySprint);
            tvEmptySprint.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    private String PresentSprint(String sprintOfRating) {
        StringTokenizer stringTokenizer = new StringTokenizer(sprintOfRating);
        return "SPRINT "+stringTokenizer.nextElement();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public interface ListenerAdapterOfRating{
        public void ActionQualify(String idOfRating, int qualify);
    }
    public interface ListenerAdapterOfRatingForQualifyFrg{
        public void AddSprintQualify(OfRating ofRating);
    }
}