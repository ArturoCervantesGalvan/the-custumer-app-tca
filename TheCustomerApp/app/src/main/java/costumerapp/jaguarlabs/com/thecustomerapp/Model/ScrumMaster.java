package costumerapp.jaguarlabs.com.thecustomerapp.Model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.DetailsProjectFragment;

/**
 * Created by Arturo on 13/01/2016.
 */
public class ScrumMaster {
    @SerializedName(DetailsProjectFragment.nameScrumMaster)
    String NamesScrumMaster;
    @SerializedName(DetailsProjectFragment.rolSrumMaster)
    String RolScrumMaster;
    @SerializedName(DetailsProjectFragment.emailSrumMaster)
    String EmailScrumMaster;

    //Load Data of the Contact of the Scrumm Master
    public ScrumMaster(JSONObject contacto) throws JSONException {
        this.NamesScrumMaster = contacto.getString("nombre");
        this.RolScrumMaster = contacto.getString("rol");
        this.EmailScrumMaster = contacto.getString("email");
    }

    public String getNamesScrumMaster() {
        return NamesScrumMaster;
    }

    public void setNamesScrumMaster(String namesScrumMaster) {
        NamesScrumMaster = namesScrumMaster;
    }

    public String getRolScrumMaster() {
        return RolScrumMaster;
    }

    public void setRolScrumMaster(String rolScrumMaster) {
        RolScrumMaster = rolScrumMaster;
    }

    public String getEmailScrumMaster() {
        return EmailScrumMaster;
    }

    public void setEmailScrumMaster(String emailScrumMaster) {
        EmailScrumMaster = emailScrumMaster;
    }
}
