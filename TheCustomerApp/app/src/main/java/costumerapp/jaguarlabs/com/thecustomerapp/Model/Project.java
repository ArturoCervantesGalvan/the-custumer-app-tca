package costumerapp.jaguarlabs.com.thecustomerapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 11/01/2016.
 */
public class Project implements Parcelable {
    @SerializedName(Util.idProject)
    String IdProject;
    @SerializedName(Util.nameProject)
    String NameProject;
    @SerializedName(Util.startProjectDate)
    String StartDate;
    @SerializedName(Util.endProjectDate)
    String EndDate;
    @SerializedName(Util.imageProject)
    String UrlImagen;

    //Objet for use the list Project assigned to User
    public Project() {
    }

    public String getUrlImagen() {
        return UrlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        UrlImagen = urlImagen;
    }

    public String getIdProject() {
        return IdProject;
    }

    public void setIdProject(String idProject) {
        IdProject = idProject;
    }

    public String getNameProject() {
        return NameProject;
    }

    public void setNameProject(String nameProject) {
        NameProject = nameProject;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public Project(Parcel in) {
        IdProject = in.readString();
        NameProject = in.readString();
        StartDate = in.readString();
        EndDate = in.readString();
        UrlImagen = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(IdProject);
        dest.writeString(NameProject);
        dest.writeString(StartDate);
        dest.writeString(EndDate);
        dest.writeString(UrlImagen);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Project> CREATOR = new Parcelable.Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel in) {
            return new Project(in);
        }

        @Override
        public Project[] newArray(int size) {
            return new Project[size];
        }
    };
}
