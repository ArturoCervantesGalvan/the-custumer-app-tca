package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

import com.google.gson.annotations.SerializedName;

import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 11/02/2016.
 */
public class ListProjectRequest extends BaseRequest {
    @SerializedName(Util.idUser)
    String idUser;

    public ListProjectRequest(String request, String idUser){
        this.request = request;
        this.idUser = idUser;
    }

    public String getIdUser() {
        return idUser;
    }
}
