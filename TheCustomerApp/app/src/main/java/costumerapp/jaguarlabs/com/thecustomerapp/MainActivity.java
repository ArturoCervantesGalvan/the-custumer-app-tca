package costumerapp.jaguarlabs.com.thecustomerapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import costumerapp.jaguarlabs.com.thecustomerapp.Adapter.AdapterOfRating;
import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventErrorServer;
import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventLoginSuccess;
import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.ConfirmQualifyFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.ListProjectFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.LoaderFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.LoginFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.LogoutFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.QualifyFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.SplashScreenFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.DetailsProject;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.OfRating;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.Project;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.Technology;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.User;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

public class MainActivity extends AppCompatActivity implements AdapterOfRating.ListenerAdapterOfRating {
    //initial variables
    public static final String loginFragment = "loginFragment";
    public static final String listProjectFragments = "listProjectFragment";
    private static final long SPLASH_SCREEN_DELAY = 3000;
    String username;
    Bundle args;
    ListProjectFragment listProjectFragment;

    Toolbar toolbar;
    //ActionBar actionBar;
    User user;
    SplashScreenFragment splash = new SplashScreenFragment();
    ArrayList<Project> ListProjects = new ArrayList<Project>();
    //List Qualify
    ArrayList<OfRating> ListOfRatingIOS = new ArrayList<>();
    ArrayList<OfRating> ListOfRatingAndroid = new ArrayList<>();
    ArrayList<OfRating> ListOfRatingWeb = new ArrayList<>();
    ArrayList<String> TechnologyOfRating = new ArrayList<>();
    //List History
    ArrayList<OfRating> ListOfRatingHistoryIOS = new ArrayList<>();
    ArrayList<OfRating> ListOfRatingHistoryAndroid = new ArrayList<>();
    ArrayList<OfRating> ListOfRatingHistoryWeb = new ArrayList<>();
    ArrayList<String> TechnologyOfRatingHistory = new ArrayList<>();
    IntentFilter intentFilter = new IntentFilter();
    boolean AddFilter = true;

    BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Util.Message("Llego a recibido");
            switch (intent.getAction()) {
                case Util.CodeServiceLogin:
                    //Start petition of listview
                    Util.Message("LLegamos a interpretar informacion");
                    /*try {
                        //ActionLogin(intent.getStringExtra("json"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    break;
                //Create listview of project
                case Util.CodeServiceListProject:// actionBar.setTitle(R.string.project);
                    /*try {
                        ActionListProject(intent.getStringExtra("json"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    break;
                //Refresh ListView of Project
                case Util.CodeServiceListProjectRefresh:// actionBar.setTitle(R.string.project);
                    /*try {
                        listProjectFragment.RefreshList(intent.getStringExtra("json"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;*/
                //Create Details Project
                case Util.CodeServiceDetailsProject:
                   /* Util.Message("Comienza el DEtails de Projecto");
                    try {
                        ActionDetailsProject(intent.getStringExtra("json"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;*/
                //Create list of qualify
                case Util.CodeServiceListQualify:
                    Util.Message("Comienza listado de Qualify for evaluation");
                    try {
                        ActionListQualify(intent.getStringExtra("json"), intent.getStringExtra("2ndJson"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                //action of qualify
                case Util.CodeServiceActionQualify:
                    Util.Message("Comienza la accion de evaluar");
                    try {
                        ActionOfQualifyProject(intent.getStringExtra("json"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                //Restore Password
                //Error general
                default:
                    Util.Message("LLegamos aun error");
                    ErrorInternet();
                    break;
            }
        }
    };

    //Start Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        //add the main layout
        setContentView(R.layout.activity_main);
        //add layout SplashScreen
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fgMain, splash)
                .addToBackStack(null)
                .commit();
        //start function of remove SplashScreen
        RemoveSplash();
    }

    //On Resmun Activiti
    @Override
    protected void onResume() {
        super.onResume();
        //Validation and init for the intent filters one time.
        if (AddFilter) {
            intentFilter.addAction(Util.CodeServiceLogin);
            intentFilter.addAction(Util.CodeServiceListQualify);
            intentFilter.addAction(Util.CodeServiceActionQualify);
            intentFilter.addAction(Util.CodeServiceDetailsProject);
            intentFilter.addAction(Util.CodeServiceListProject);
            intentFilter.addAction(Util.CodeServiceListProjectRefresh);
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(myReceiver, intentFilter);
            Util.Message("Se agregan los filters");
        }
    }

    //On Pause Activity
    @Override
    protected void onPause() {
        //On Pause for no renew of the intent filter
        super.onPause();
        AddFilter = false;
        Util.Message("LLegamos a Pausa");
    }

    //On Destroy Activity
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        //Clear all Arrya List
        ClearAllArrayList();
        //remove instance of the intente filter
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(myReceiver);
    }

    //overwriting method press back
    @Override
    public void onBackPressed() {
        //Validations of the fragments
        Util.Message(String.valueOf(getSupportFragmentManager().getBackStackEntryCount()));
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            //Clear of the Array List
            ClearAllArrayList();
            //ActiveLogin();
        } else {
            finish();
        }
    }

    //Action with behaivors of button
    //cancel Action of the Restore Password and confirm message
    public void CancelAction(View view) {
        getSupportFragmentManager().popBackStack();
    }


    /*All function of Petition */

    //Customer project list
    public void CustomerProjectList(User user) {
        //init Services
        Intent intent = new Intent(getApplicationContext(), ConnectionServiceAndroid.class);
        //setAction
        intent.setAction(Util.CodeServiceLogin);
        //Parameters
        intent.putExtra("parameters", "{\"solicitud\":\"listado\",\"id_usuario\":\"" + user.getIdUser() + "\"}");
        Util.Message(intent.getStringExtra("parameters"));
        intent.putExtra("action", Util.CodeServiceListProject);
        intent.putExtra("url", Util.urlListProject);
        Util.Message("Comienza Servicio");
        //Star Services
        startService(intent);
    }

    //Refresh List the Project
    public void CustomerProjectList() {
        //init service
        Intent intent = new Intent(getApplicationContext(), ConnectionServiceAndroid.class);
        //set Parameters
        intent.setAction(Util.CodeServiceLogin);
        intent.putExtra("parameters", "{\"solicitud\":\"listado\",\"id_usuario\":\"" + user.getIdUser() + "\"}");
        Util.Message(intent.getStringExtra("parameters"));
        intent.putExtra("action", Util.CodeServiceListProjectRefresh);
        intent.putExtra("url", Util.urlListProject);
        Util.Message("Comienza Servicio");
        //Start Services
        startService(intent);
    }

    //Function of Login of the App
    public void Login(View view) {
        //init of the data.
        /*etUsername = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        //Validation of the data
        if (Util.Validation(etUsername.getText().toString(), etPassword.getText().toString())) {
            //init services
            Intent intent = new Intent(getApplicationContext(), ConnectionServiceAndroid.class);
            //set Parameters
            username = etUsername.getText().toString();
            intent.setAction(Util.CodeServiceLogin);
            intent.putExtra("parameters", "{\"login\":\"" + etUsername.getText().toString() + "\",\"password\":\"" + Util.encrypted(etPassword.getText().toString()) + "\"}");
            Util.Message(intent.getStringExtra("parameters"));
            intent.putExtra("action", Util.CodeServiceLogin);
            intent.putExtra("url", Util.urlLogin);
            Util.Message("Comienza Servicio");
            //Start Services
            startService(intent);
        } else {
            //Error validate of date.
            ErrorLogin();
        }*/
    }


    //Function of Details Project
    public void RequestDeatilsProject(String IdProject) {
        //init services
        Intent intent = new Intent(getApplicationContext(), ConnectionServiceAndroid.class);
        //Set Parameters
        intent.setAction(Util.CodeServiceDetailsProject);
        intent.putExtra("parameters", "{\"solicitud\":\"detalle\",\"id_proyecto\":\"" + IdProject + "\",\"token\":\"" + user.getToken() + "\",\"refresh_token\":\"" + user.getRefreshToken() + "\"}");
        Util.Message(intent.getStringExtra("parameters"));
        intent.putExtra("action", Util.CodeServiceDetailsProject);
        intent.putExtra("url", Util.urlDetailsProject);
        Util.Message("Comienza Servicio");
        //Start Services
        startService(intent);
    }

    //Function Details of the Qualify Project
    public void RequestQualifyProject(String IdProject, String AppName) {
        //init services
        Intent intent = new Intent(getApplicationContext(), ConnectionServiceAndroid.class);
        //set Parameters
        intent.setAction(Util.CodeServiceDetailsProject);
        intent.putExtra("parameters", "{\"solicitud\":\"por_evaluar\",\"id_proyecto\":\"" + IdProject + "\",\"token\":\"" + user.getToken() + "\",\"refresh_token\":\"" + user.getRefreshToken() + "\"}");
        Util.Message(intent.getStringExtra("parameters"));
        intent.putExtra("2ndParameters", "{\"solicitud\":\"historial_evaluaciones\",\"id_proyecto\":\"" + IdProject + "\",\"token\":\"" + user.getToken() + "\",\"refresh_token\":\"" + user.getRefreshToken() + "\"}");
        intent.putExtra("action", Util.CodeServiceListQualify);
        Util.Message(intent.getStringExtra("2ndParameters"));
        intent.putExtra("url", Util.urlListProject);
        Util.Message("Comienza Servicio");
        //Start Services
        startService(intent);
    }

    /*public void ActionQualify(String idOfRating, int getStar) {
        //init Services
        Intent intent = new Intent(getApplicationContext(), ConnectionServiceAndroid.class);
        //Set Parameters
        intent.setAction(Util.CodeServiceActionQualify);
        intent.putExtra("parameters", "{\"solicitud\":\"evaluar\",\"id\":\"" + idOfRating + "\",\"calificacion\":" + getStar + ",\"token\":\"" + user.getToken() + "\",\"refresh_token\":\"" + user.getRefreshToken() + "\"}");
        Util.Message(intent.getStringExtra("parameters"));
        intent.putExtra("action", Util.CodeServiceActionQualify);
        intent.putExtra("url", Util.urlQualifyProject);
        Util.Message("Comienza Servicio");
        //Start Services
        startService(intent);
    }*/



    //Load Fragment whit list Project
    /*public void LoadListFragments(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArrayProject = jsonObject.getJSONArray("proyectos");
        Util.Message("LLego el JSon para el Arrya List");
        //Begin tour of JSONArray
        if (jsonArrayProject.length() != 0) {
            Util.Message("Comienza el Recorrido");
            if (ListProjects.size() != 0)
                ListProjects.clear();
            for (int i = 0; i < jsonArrayProject.length(); i++) {
                //Load Objet in the Array List
                Project project = new Project();
                JSONObject jsonProject = jsonArrayProject.getJSONObject(i);
                project.setIdProject(jsonProject.getString("id_proyecto"));
                project.setNameProject(jsonProject.getString("nombre"));
                project.setStartDate(Util.FormatDate(jsonProject.getString("fecha_inicio")));
                project.setEndDate(Util.FormatDate(jsonProject.getString("fecha_finalizacion")));
                project.setUrlImagen(jsonProject.getString("imagen"));
                ListProjects.add(project);
            }//End tour of JSonArray
            Util.Message("Finalizo el recorrido");
            //Create Arguments
            args = new Bundle();
            args.putParcelableArrayList("ArraylistProject", ListProjects);
            //Create Objet Fragments
            listProjectFragment = new ListProjectFragment();
            //Load Arguments
            listProjectFragment.setArguments(args);
            Util.Message("Crearon Argumentos");
            //Remove current Fragment
            getSupportFragmentManager().popBackStack();
            //Add Fragment List Project
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fgMain, listProjectFragment)
                    .addToBackStack(null)
                    .commit();
            initActionBard();
            SetTitleToolbar(R.string.project_title);
            Util.Message("Se Cargo el Layout de listado de projectos");

        } else {
            //unexpected error
            Util.Message("Array Vacio");
            ErrorInternet();
        }
    }*/

    //function of Remove Splash
    public void RemoveSplash() {
        //funtion Remove SplashScrenn
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                getSupportFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fgMain, new LoginFragment(),MainActivity.loginFragment)
                        .addToBackStack(null)
                        .commit();

            }
        };
        Timer timer = new Timer();
        //Star Thread for Remove SplashScren
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }

    //Action of qualify project
    private void ActionOfQualifyProject(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        Util.Message(jsonObject.getString("codigo"));
        switch (jsonObject.getString("codigo")) {
            //Correct Answer
            case Util.codeRequestAfirm:
                //Add fragment of confirm
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fgMain, new ConfirmQualifyFragment())
                        .addToBackStack(null)
                        .commit();
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //Bad request
            case Util.codeBadRequest:
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //Insuficent Data
            case Util.codeInsufficientData:
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //Unexpected error
            default:
                Util.Message("Error aun mas inesperado");
                break;
        }
    }

    //Action of List of Qualify project
    private void ActionListQualify(String json, String Json2nd) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        JSONObject jsonObject2ndPetition = new JSONObject(Json2nd);
        Util.Message("Comienza la accion de Detalles ");
        Util.Message(jsonObject.getString("codigo"));
        //Evaluation of the response
        switch (jsonObject.getString("codigo")) {
            //correct Answer
            case Util.codeRequestAfirm:
                //init JSON
                JSONArray jsonArrayOfRating = jsonObject.getJSONArray("por_calificar");
                JSONArray jsonArrayHistory = jsonObject2ndPetition.getJSONArray("historial");
                QualifyFragment qualifyFragment = new QualifyFragment();
                //add Arguments
                args.putAll(AddArgumentsQualific(jsonArrayOfRating));
                args.putAll(AddArgumentsQualificHistory(jsonArrayHistory));
                //set Arguments
                qualifyFragment.setArguments(args);
                Util.Message("Creamos el nuevo fragment de qualify");
                //add fragment
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fgMain, qualifyFragment)
                        .addToBackStack(null)
                        .commit();
                SetTitleToolbar(R.string.qualify_title);
                break;
            //Bad Request
            case Util.codeBadRequest:
                Util.Message("Error en la petición de la listado de Projecto");
                ErrorInternet();
                break;
            //Insufficent Data
            case Util.codeInsufficientData:
                Util.Message("Error en falta de datos");
                ErrorInternet();
                break;
            //Unexpected Error
            default:
                Util.Message("Error aun mas inesperado");
                ErrorInternet();
                break;
        }
    }

    //Action  of Response of the DetailsProject
    private void ActionDetailsProject(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        Util.Message(jsonObject.getString("codigo"));
        switch (jsonObject.getString("codigo")) {
            //Correct Answer
            case Util.codeRequestAfirm:
                //Create Object for use the Details Project
                Util.Message("Creamos EL nuevo fragment");
                JSONObject jsonObjectProjecto = jsonObject.getJSONObject("proyecto");
                Util.Message(jsonObjectProjecto.getString("nombre"));
                SetTitleToolbar(jsonObjectProjecto.getString("nombre"));
                //Create Object of the DetailsProject
                DetailsProject detailsProject = new DetailsProject(jsonObjectProjecto);
                //Create Object of the DetailsProjectFragment with base of the object DetailsProject
                //DetailsProjectFragment detailsProjectFragment = new DetailsProjectFragment(detailsProject);
                //Init of the ArrayList of the Technology object
                ArrayList<Technology> ListTechnology = new ArrayList<>();
                Util.Message("Se creo la lista");
                //Init of the JSONArray for Technology
                JSONArray jsonArrayTechnology = jsonObjectProjecto.getJSONArray("tecnologias");
                if (jsonArrayTechnology.length() != 0) {
                    //Start JSONArray
                    for (int i = 0; i < jsonArrayTechnology.length(); i++) {
                        Technology technology = new Technology();
                        //Create JSONObject in base JSONArray
                        JSONObject jsonObjectTechnology = jsonArrayTechnology.getJSONObject(i);
                        //Add the Data for the Object
                        technology.setIdTechnology(jsonObjectTechnology.getString("id"));
                        technology.setTechnologyName(jsonObjectTechnology.getString("tecnologia"));
                        technology.setNextDelivery(Util.FormatDate(jsonObjectTechnology.getString("proxima_entrega")));
                        technology.setRestrictions(jsonObjectTechnology.getString("restricciones"));
                        technology.setSprint(jsonObjectTechnology.getString("sprint"));
                        //Format of the date
                        technology.setStartSprint(Util.FormatDate(jsonObjectTechnology.getString("inicio")));
                        technology.setStatus(jsonObjectTechnology.getString("estatus"));
                        technology.setType(jsonObjectTechnology.getString("tipo"));
                        //add the List
                        ListTechnology.add(technology);
                    }
                }
                //Create Arguments
                Bundle args = new Bundle();
                args.putParcelableArrayList("ListOfTechnology", ListTechnology);
                //Load Arguments
                //detailsProjectFragment.setArguments(args);
                Util.Message("Se cargo la lista");

                //Add Arguments to Fragments
                getSupportFragmentManager().beginTransaction()
                        //.add(R.id.fgMain, detailsProjectFragment)
                        .addToBackStack(null)
                        .commit();
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //Bad Request
            case Util.codeBadRequest:
                ErrorInternet();
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //Insufficient data
            case Util.codeInsufficientData:
                ErrorInternet();
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //Unexpected Error
            default:
                ErrorInternet();
                break;
        }
    }

    //Action Of List Project
    private void ActionListProject(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        switch (jsonObject.getString("codigo")) {
            //Answer Affirm
            case Util.codeRequestAfirm:
                //LoadListFragments(jsonObject);
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //Bad Request
            case Util.codeBadRequest:
                ErrorInternet();
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //InsufficentData
            case Util.codeInsufficientData:
                ErrorInternet();
                Util.Message(jsonObject.getString("mensaje"));
                break;
            //unexpected Error
            default:
                ErrorInternet();
                Util.Message("Error aun mas inesperado");
                break;
        }
    }

    //Action Login
   /* private void ActionLogin(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        switch (jsonObject.getString("codigo")) {
            //Correct Answer
            case Util.codeRequestAfirm:
                user = new User(jsonObject.getJSONObject("usuario"));
                Util.Message("Start action of list Proyect");
                CustomerProjectList(user);
                break;
            //Error of Autetification
            case Util.codeAuthenticationError:
                ErrorLogin();
                break;
            //Error of insufficient data
            default:
                Util.Message("Error aun mas inexperado");
                ErrorInternet();
                break;
        }
    }*/

    //Add title to Toolbar
    private void SetTitleToolbar(int project_title) {
        setTitle(project_title);
    }

    private void SetTitleToolbar(String title_toolbar) {
        setTitle(title_toolbar);
    }

    //init toolbard
    private void initActionBard() {
        //init Toolbard
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbToolbar);
        //Set Parameters
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitleTextColor(getResources().getColor(R.color.WHITE));
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.icon_overflow));
        toolbar.setLogo(R.drawable.icon_garra_default);
        setSupportActionBar(toolbar);
    }

    //Load Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_action_menu, menu);
        return true;
    }

    //actions of the menu  ****Pendiente****
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //Event for send Mail
            case R.id.itEmail:
                String[] emails = new String[]{Util.EmailSupport};
                Util.Message("Mensaje al SrummMaster");
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, emails);
                intent.setType("message/rfc822");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(Intent.createChooser(intent, "Send mail..."));
                }
                return true;
            //Event of the Show login
            case R.id.itLogout:
                Util.Message("Salir de la App");
                LogoutFragment logoutFragment = new LogoutFragment();
                args = new Bundle();
                args.putString("Email", username);
                logoutFragment.setArguments(args);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fgMain, logoutFragment)
                        .addToBackStack(null)
                        .commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //Alerts

    /*Application response will be left without internet in the middle of the process.*/
    public void ErrorInternet() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.error_internet_connection, (ViewGroup) findViewById(R.id.llErrorInternet));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 250);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //Application response for error log.
    public void ErrorLogin() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_layout_toast, (ViewGroup) findViewById(R.id.llCustomToast));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 270);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //Restore error response via email password
    public void ErrorEmailRestored() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_layout_toast_error_mail, (ViewGroup) findViewById(R.id.llCustomToast));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 250);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //Me quede aqui

    public Bundle AddArgumentsQualific(JSONArray jsonArrayOfRating) throws JSONException {
        //init start cycle
        for (int i = 0; i < jsonArrayOfRating.length(); i++) {
            //init object
            JSONObject jsonObjectOfRating = jsonArrayOfRating.getJSONObject(i);
            OfRating ofRating = new OfRating();
            //validation of data and upload to object
            if (jsonObjectOfRating.has("id"))
                ofRating.setIdOfRating(jsonObjectOfRating.getString("id"));
            if (jsonObjectOfRating.has("tecnologia"))
                ofRating.setTechnologyOfRating(jsonObjectOfRating.getString("tecnologia"));
            if (jsonObjectOfRating.has("sprint")) {
                ofRating.setSprintOfRating(jsonObjectOfRating.getString("sprint"));
                ofRating.setOrderOfQuality(PaserToIntSprint(jsonObjectOfRating.getString("sprint")));
            }
            if (jsonObjectOfRating.has("restricciones"))
                ofRating.setRestrictionsOfRating(jsonObjectOfRating.getString("restricciones"));
            if (jsonObjectOfRating.has("inicio"))
                ofRating.setStartDateOfRating(jsonObjectOfRating.getString("inicio"));
            if (jsonObjectOfRating.has("proxima_etrega"))
                ofRating.setNextDeliveryOfRating(jsonObjectOfRating.getString("proxima_entrega"));
            if (jsonObjectOfRating.has("estatus"))
                ofRating.setStatusOfRating(jsonObjectOfRating.getString("estatus"));
            if (jsonObjectOfRating.has("tipo"))
                ofRating.setTypeOfRating(jsonObjectOfRating.getString("tipo"));
            //select to collection by the Technology
            switch (ofRating.getTechnologyOfRating()) {
                case "iOS": //Util.Message("Se agrega a la lista por evaluar de IOS");
                    ListOfRatingIOS.add(ofRating);
                    break;
                case "Android":// Util.Message("Se agrega a la lista por evaluar de Android");
                    ListOfRatingAndroid.add(ofRating);
                    break;
                case "Web": //Util.Message("Se agrega a la lista por evaluar de Web");
                    ListOfRatingWeb.add(ofRating);
                    break;
                default:
                    Util.Message("Mensaje aun mas inesperado");
                    break;
            }
        }
        /*Only for validation upload data*/
        if (ListOfRatingWeb.size() != 0)
            Util.Message("Tenemos estos de Web " + ListOfRatingWeb.size() + " ");
        if (ListOfRatingAndroid.size() != 0)
            Util.Message("Tenemos estos de Android " + ListOfRatingAndroid.size() + " ");
        if (ListOfRatingIOS.size() != 0)
            Util.Message("Tenemos estos de IOS " + ListOfRatingIOS.size() + " ");
        /*End of the Validation*/
        Bundle ListArgumentsQualify = new Bundle();
        if (TechnologyOfRating != null)
            TechnologyOfRating.clear();
        Util.Message("Limpiamos la lista de padres");
        //Upload of the parents
        if (ListOfRatingIOS.size() != 0) {
            TechnologyOfRating.add("IOS");
            //Order of List
            Collections.sort(ListOfRatingIOS, new Comparator<OfRating>() {
                @Override
                public int compare(OfRating lhs, OfRating rhs) {
                    return Integer.valueOf(lhs.getOrderOfQuality()).compareTo(rhs.getOrderOfQuality());
                }
            });
            ListArgumentsQualify.putParcelableArrayList("ListOfRatingIOS", ListOfRatingIOS);
            Util.Message("Se agrego el argumento de IOS");
        }
        if (ListOfRatingAndroid.size() != 0) {
            TechnologyOfRating.add("Android");
            Collections.sort(ListOfRatingAndroid, new Comparator<OfRating>() {
                @Override
                public int compare(OfRating lhs, OfRating rhs) {
                    return Integer.valueOf(lhs.getOrderOfQuality()).compareTo(rhs.getOrderOfQuality());
                }
            });
            ListArgumentsQualify.putParcelableArrayList("ListOfRatingAndroid", ListOfRatingAndroid);
            Util.Message("Se agrego el argumento de Android");
        }
        if (ListOfRatingWeb.size() != 0) {
            TechnologyOfRating.add("Web");
            Collections.sort(ListOfRatingWeb, new Comparator<OfRating>() {
                @Override
                public int compare(OfRating lhs, OfRating rhs) {
                    return Integer.valueOf(lhs.getOrderOfQuality()).compareTo(rhs.getOrderOfQuality());
                }
            });
            ListArgumentsQualify.putParcelableArrayList("ListOfRatingWeb", ListOfRatingWeb);
            Util.Message("Se agrego el argumento de WEB");
        }
        //Send the Data
        Util.Message("Le mando estas");
        Util.Message(String.valueOf(TechnologyOfRating.size()));
        ListArgumentsQualify.putStringArrayList("TechnologyOfRating", TechnologyOfRating);
        Util.Message("Se agrega la lista de Tecnologias");
        return ListArgumentsQualify;
    }
    //User for order the Sprint
    private int PaserToIntSprint(String sprint) {
        StringTokenizer stringTokenizer = new StringTokenizer(sprint);
        return Integer.parseInt(stringTokenizer.nextToken());
    }

    public Bundle AddArgumentsQualificHistory(JSONArray jsonArrayOfRating) throws JSONException {
        //init of the cycle
        for (int i = 0; i < jsonArrayOfRating.length(); i++) {
            //init the object
            JSONObject jsonObjectOfRating = jsonArrayOfRating.getJSONObject(i);
            OfRating ofRating = new OfRating();
            //Validations and uploads the data
            if (jsonObjectOfRating.has("id"))
                ofRating.setIdOfRating(jsonObjectOfRating.getString("id"));
            if (jsonObjectOfRating.has("tecnologia"))
                ofRating.setTechnologyOfRating(jsonObjectOfRating.getString("tecnologia"));
            if (jsonObjectOfRating.has("sprint")) {
                ofRating.setSprintOfRating(jsonObjectOfRating.getString("sprint"));
                ofRating.setOrderOfQuality(PaserToIntSprint(jsonObjectOfRating.getString("sprint")));
            }
            if (jsonObjectOfRating.has("calificacion"))
                ofRating.setQualifyOfSprint(jsonObjectOfRating.getInt("calificacion"));
            if (jsonObjectOfRating.has("restricciones"))
                ofRating.setRestrictionsOfRating(jsonObjectOfRating.getString("restricciones"));
            if (jsonObjectOfRating.has("inicio"))
                ofRating.setStartDateOfRating(jsonObjectOfRating.getString("inicio"));
            if (jsonObjectOfRating.has("proxima_entrega"))
                ofRating.setNextDeliveryOfRating(jsonObjectOfRating.getString("proxima_entrega"));
            if (jsonObjectOfRating.has("estatus"))
                ofRating.setStatusOfRating(jsonObjectOfRating.getString("estatus"));
            if (jsonObjectOfRating.has("tipo"))
                ofRating.setTypeOfRating(jsonObjectOfRating.getString("tipo"));
            //Select of the collections.
            switch (ofRating.getTechnologyOfRating()) {
                case "iOS":
                    Util.Message("Se agrega a la lista por evaluar de IOS historial");
                    ListOfRatingHistoryIOS.add(ofRating);
                    break;
                case "Android":
                    Util.Message("Se agrega a la lista por evaluar de Android historial");
                    ListOfRatingHistoryAndroid.add(ofRating);
                    break;
                case "Web":
                    Util.Message("Se agrega a la lista por evaluar de Web historial");
                    ListOfRatingHistoryWeb.add(ofRating);
                    break;
                default:
                    Util.Message("Mensaje aun mas inesperado");
                    break;
            }
        }
        //order of the collection
        Bundle ListArgumentsQualify = new Bundle();
        if (TechnologyOfRatingHistory != null)
            TechnologyOfRatingHistory.clear();
        if (ListOfRatingHistoryIOS.size() != 0) {
            TechnologyOfRatingHistory.add("IOS");
            Collections.sort(ListOfRatingHistoryIOS, new Comparator<OfRating>() {
                @Override
                public int compare(OfRating lhs, OfRating rhs) {
                    return Integer.valueOf(lhs.getOrderOfQuality()).compareTo(rhs.getOrderOfQuality());
                }
            });
            ListArgumentsQualify.putParcelableArrayList("ListOfRatingHistoryIOS", ListOfRatingHistoryIOS);
            Util.Message("Se agrego el argumento de IOS historial");
        }
        if (ListOfRatingHistoryAndroid.size() != 0) {
            TechnologyOfRatingHistory.add("Android");
            Collections.sort(ListOfRatingHistoryAndroid, new Comparator<OfRating>() {
                @Override
                public int compare(OfRating lhs, OfRating rhs) {
                    return Integer.valueOf(lhs.getOrderOfQuality()).compareTo(rhs.getOrderOfQuality());
                }
            });
            ListArgumentsQualify.putParcelableArrayList("ListOfRatingHistoryAndroid", ListOfRatingHistoryAndroid);
            Util.Message("Se agrego el argumento de Android historial");
        }
        if (ListOfRatingHistoryWeb.size() != 0) {
            TechnologyOfRatingHistory.add("Web");
            Collections.sort(ListOfRatingHistoryWeb, new Comparator<OfRating>() {
                @Override
                public int compare(OfRating lhs, OfRating rhs) {
                    return Integer.valueOf(lhs.getOrderOfQuality()).compareTo(rhs.getOrderOfQuality());
                }
            });
            ListArgumentsQualify.putParcelableArrayList("ListOfRatingHistoryWeb", ListOfRatingHistoryWeb);
            Util.Message("Se agrego el argumento de WEB historial");
        }
        ListArgumentsQualify.putStringArrayList("TechnologyOfRatingHistory", TechnologyOfRatingHistory);
        Util.Message("****Se enviasn Tecnologias: "+TechnologyOfRatingHistory.size() +" historial");
        return ListArgumentsQualify;
    }
    //Show login
    public void ShowLogin() {
        ClearAllArrayList();
        int count = getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; ++i) {
            getSupportFragmentManager().popBackStack();
        }
        /*getSupportFragmentManager().beginTransaction()
                .add(R.id.fgMain, new LoginFragment(), MainActivity.loginFragment)
                .addToBackStack(null)
                .commit();*/
        toolbar = (Toolbar) findViewById(R.id.tbToolbar);
        toolbar.setVisibility(View.GONE);
    }
    //Clear the Array List
    public void ClearAllArrayList() {
        if (ListOfRatingIOS.size() != 0)
            ListOfRatingIOS.clear();
        if (ListOfRatingAndroid.size() != 0)
            ListOfRatingAndroid.clear();
        if (ListOfRatingWeb.size() != 0)
            ListOfRatingWeb.clear();
        if (TechnologyOfRating.size() != 0)
            TechnologyOfRating.clear();
        if (ListOfRatingHistoryIOS.size() != 0)
            ListOfRatingHistoryIOS.clear();
        if (ListOfRatingHistoryAndroid.size() != 0)
            ListOfRatingHistoryAndroid.clear();
        if (ListOfRatingHistoryWeb.size() != 0)
            ListOfRatingHistoryWeb.clear();
        if (TechnologyOfRatingHistory.size() != 0)
            TechnologyOfRatingHistory.clear();
    }

    @Override
    public void ActionQualify(String idOfRating, int qualify) {
        //init Services
        Intent intent = new Intent(getApplicationContext(), ConnectionServiceAndroid.class);
        //Set Parameters
        intent.setAction(Util.CodeServiceActionQualify);
        intent.putExtra("parameters", "{\"solicitud\":\"evaluar\",\"id\":\"" + idOfRating + "\",\"calificacion\":" + qualify + ",\"token\":\"" + user.getToken() + "\",\"refresh_token\":\"" + user.getRefreshToken() + "\"}");
        Util.Message(intent.getStringExtra("parameters"));
        intent.putExtra("action", Util.CodeServiceActionQualify);
        intent.putExtra("url", Util.urlQualifyProject);
        Util.Message("Comienza Servicio");
        //Start Services
        startService(intent);
    }

    @Subscribe
    public void onEvent(EventLoginSuccess eventLoginSuccess){
        Util.Message("LLego el evento");
        Util.Message(eventLoginSuccess.getUser().getFullUsername());
        Util.tokenUserLogin = eventLoginSuccess.getUser().getToken();
        Util.refreshTokenUserLogin = eventLoginSuccess.getUser().getRefreshToken();
        ListProjectFragment listProjectFragment = new ListProjectFragment();
        listProjectFragment.setUser(eventLoginSuccess.getUser());
        getSupportFragmentManager().popBackStack();
        getSupportFragmentManager().executePendingTransactions();
        initActionBard();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fgMain,listProjectFragment,listProjectFragments)
                .addToBackStack(null)
                .commit();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fgMain, new LoaderFragment(), Util.loaderFragments)
                .addToBackStack(null)
                .commit();
        getSupportFragmentManager().executePendingTransactions();
    }
    @Subscribe
    public void onEvent(EventErrorServer eventErrorServer){
        View layout = getLayoutInflater()
                .inflate(R.layout.error_internet_connection, (ViewGroup) findViewById(R.id.llErrorInternet));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 250);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        Util.Message(eventErrorServer.getMessageServer());
    }
}
