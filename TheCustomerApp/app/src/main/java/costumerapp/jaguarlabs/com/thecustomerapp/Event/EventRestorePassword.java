package costumerapp.jaguarlabs.com.thecustomerapp.Event;

/**
 * Created by Arturo on 10/02/2016.
 */
public class EventRestorePassword {
    private String email;
    private int code;
    public EventRestorePassword(String email, int code){
        this.email = email;
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public int getCode() {
        return code;
    }
}
