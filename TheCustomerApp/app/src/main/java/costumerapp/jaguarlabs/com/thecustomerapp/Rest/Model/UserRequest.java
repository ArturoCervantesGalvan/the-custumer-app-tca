package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

/**
 * Created by Arturo on 09/02/2016.
 */
public class UserRequest {
    public String login;
    public String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
