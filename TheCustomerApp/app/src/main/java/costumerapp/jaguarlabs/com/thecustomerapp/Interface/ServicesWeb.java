package costumerapp.jaguarlabs.com.thecustomerapp.Interface;

import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.BaseResponse;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.DetailsProjectRequest;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.DetailsProjectResponse;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.ListProjectRequest;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.ListProjectResponse;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.RestorePasswordRequest;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.UserRequest;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.UserResponse;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Arturo on 09/02/2016.
 */
public interface ServicesWeb {
    @POST(Util.urlLogin)
    void getUser(@Body UserRequest userRequest, Callback<UserResponse> rp);

    @POST(Util.urlRestorePassword)
    void getRestorePassword(@Body RestorePasswordRequest restorePasswordRequest, Callback<BaseResponse> rp );

    @POST(Util.urlListProject)
    void getListProject(@Body ListProjectRequest listProjectRequest, Callback<ListProjectResponse> listProjectResponseCallback);

    @POST(Util.urlDetailsProject)
    void getDetailsProject(@Body DetailsProjectRequest detailsProjectRequest, Callback<DetailsProjectResponse> detailsProjectResponseCallback);
}
