package costumerapp.jaguarlabs.com.thecustomerapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Arturo on 18/01/2016.
 */
public class OfRating implements Parcelable {
    String IdOfRating;
    String TechnologyOfRating;
    String SprintOfRating;
    String RestrictionsOfRating;
    String StartDateOfRating;
    String NextDeliveryOfRating;
    String StatusOfRating;
    String TypeOfRating;
    int QualifyOfSprint;
    int OrderOfQuality;

    public int getOrderOfQuality() {
        return OrderOfQuality;
    }

    public void setOrderOfQuality(int orderOfQuality) {
        OrderOfQuality = orderOfQuality;
    }

    public int getQualifyOfSprint() {
        return QualifyOfSprint;
    }

    public void setQualifyOfSprint(int qualifyOfSprint) {
        QualifyOfSprint = qualifyOfSprint;
    }

    public OfRating() {

    }

    public String getIdOfRating() {
        return IdOfRating;
    }

    public void setIdOfRating(String idOfRating) {
        IdOfRating = idOfRating;
    }

    public String getTechnologyOfRating() {
        return TechnologyOfRating;
    }

    public void setTechnologyOfRating(String technologyOfRating) {
        TechnologyOfRating = technologyOfRating;
    }

    public String getSprintOfRating() {
        return SprintOfRating;
    }

    public void setSprintOfRating(String sprintOfRating) {
        SprintOfRating = sprintOfRating;
    }

    public String getRestrictionsOfRating() {
        return RestrictionsOfRating;
    }

    public void setRestrictionsOfRating(String restrictionsOfRating) {
        RestrictionsOfRating = restrictionsOfRating;
    }

    public String getStartDateOfRating() {
        return StartDateOfRating;
    }

    public void setStartDateOfRating(String startDateOfRating) {
        StartDateOfRating = startDateOfRating;
    }

    public String getNextDeliveryOfRating() {
        return NextDeliveryOfRating;
    }

    public void setNextDeliveryOfRating(String nextDeliveryOfRating) {
        NextDeliveryOfRating = nextDeliveryOfRating;
    }

    public String getStatusOfRating() {
        return StatusOfRating;
    }

    public void setStatusOfRating(String statusOfRating) {
        StatusOfRating = statusOfRating;
    }

    public String getTypeOfRating() {
        return TypeOfRating;
    }

    public void setTypeOfRating(String typeOfRating) {
        TypeOfRating = typeOfRating;
    }

    public OfRating(Parcel in) {
        IdOfRating = in.readString();
        TechnologyOfRating = in.readString();
        SprintOfRating = in.readString();
        RestrictionsOfRating = in.readString();
        StartDateOfRating = in.readString();
        NextDeliveryOfRating = in.readString();
        StatusOfRating = in.readString();
        TypeOfRating = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(IdOfRating);
        dest.writeString(TechnologyOfRating);
        dest.writeString(SprintOfRating);
        dest.writeString(RestrictionsOfRating);
        dest.writeString(StartDateOfRating);
        dest.writeString(NextDeliveryOfRating);
        dest.writeString(StatusOfRating);
        dest.writeString(TypeOfRating);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OfRating> CREATOR = new Parcelable.Creator<OfRating>() {
        @Override
        public OfRating createFromParcel(Parcel in) {
            return new OfRating(in);
        }

        @Override
        public OfRating[] newArray(int size) {
            return new OfRating[size];
        }
    };
}
