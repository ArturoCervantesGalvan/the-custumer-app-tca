package costumerapp.jaguarlabs.com.thecustomerapp.Event;

import costumerapp.jaguarlabs.com.thecustomerapp.Model.User;

/**
 * Created by Arturo on 09/02/2016.
 */
public class EventLoginSuccess {
    private User user;
    public EventLoginSuccess(User user){
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
