package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

import com.google.gson.annotations.SerializedName;

import costumerapp.jaguarlabs.com.thecustomerapp.Model.DetailsProject;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 12/02/2016.
 */
public class DetailsProjectResponse extends BaseResponse {
    @SerializedName(Util.project)
    DetailsProject detailsProject;

    public DetailsProject getDetailsProject() {
        return detailsProject;
    }
}
