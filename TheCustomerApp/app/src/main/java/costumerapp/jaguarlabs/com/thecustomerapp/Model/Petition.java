package costumerapp.jaguarlabs.com.thecustomerapp.Model;

/**
 * Created by Arturo on 11/01/2016.
 */
public class Petition {
    //Pettion for use in the Connection to Service Web
    String Url;
    String Parameters;
    int CodePetition;
    String AppName;
    String Parameters2nd;

    public String getParameters2nd() {
        return Parameters2nd;
    }

    public void setParameters2nd(String parameters2nd) {
        Parameters2nd = parameters2nd;
    }

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    public int getCodePetition() {
        return CodePetition;
    }

    public void setCodePetition(int codePetition) {
        CodePetition = codePetition;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getParameters() {
        return Parameters;
    }

    public void setParameters(String parameters) {
        Parameters = parameters;
    }
}
