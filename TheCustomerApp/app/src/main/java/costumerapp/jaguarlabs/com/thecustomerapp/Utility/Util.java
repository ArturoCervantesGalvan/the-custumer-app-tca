package costumerapp.jaguarlabs.com.thecustomerapp.Utility;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.StringTokenizer;

/**
 * Created by Arturo on 06/01/2016.
 */
public class Util {
    // Constants for connection to web services
    //Variable of the App
    public static String tokenUserLogin;
    public static String refreshTokenUserLogin;
    //Url of Service Web
    public static final String baseUrl = "http://jaguarlabs.com/sistemas/acaponeta/ws";
    public static final String urlLogin = "/login.php";
    public static final String urlListProject = "/proyecto.php";
    public static final String urlQualifyProject = "/proyecto.php";
    public static final String urlQualifyProjectHistory = "/proyecto.php";
    public static final String urlRestorePassword = "/usuario.php";
    public static final String urlDetailsProject = "/proyecto.php";

    //Code of Petition of the service web
    public static final int CodePetitionLogin = 201;
    public static final int CodePetitionListProject = 202;
    public static final int CodePetitionListQualify = 203;
    public static final int CodePetitionActionQualify = 204;
    //public static final int CodePetitionHistory = 205;
    public static final int CodePetitionRestorePassword = 206;
    public static final int CodePetitonDetailsProject = 207;

    //Code of petition of the service android
    public static final String message = "mensaje";
    public static final String code = "codigo";
    public static final String request = "solicitud";
    public static final String confirm = "confirm";
    public static final String canceled = "cancel";
    public static final String retry = "retry";
    public static final String loaderFragments = "loaderFragment";

    //Constact of the LoginFragment Request and Response
    public static final String idUser = "id_usuario";
    public static final String token = "token";
    public static final String refreshToken = "refresh_token";

    //constact of the List Project
    public static final String projects = "proyectos";
    public static final String project = "proyecto";
    public static final String idProject = "id_proyecto";
    public static final String nameProject = "nombre";
    public static final String startProjectDate = "fecha_inicio";
    public static final String endProjectDate = "fecha_finalizacion";
    public static final String imageProject = "imagen";
    public static final String technologyApp = "tecnologias";

    //Constact of Scrum Master


    public static final String CodeServiceLogin = "201";
    public static final String CodeServiceListProject = "202";
    public static final String CodeServiceListQualify = "203";
    public static final String CodeServiceActionQualify = "204";
    //public static final int CodePetitionHistory = 205;
    public static final String CodeServiceRestorePassword = "206";
    public static final String CodeServiceDetailsProject = "207";
    public static final String CodeServiceListProjectRefresh = "208";

    //Code JSONObject Response
    public static final String codeRequestAfirm = "200";
    public static final String codeBadRequest = "300";
    public static final String codeInsufficientData = "301";
    public static final String codeAuthenticationError = "302";
    public static final String codeFailedToSendEmail = "303";

    //Code of request of Action login
    public static final int CodeLoginAfirm = 200;
    public static final int CodeLoginError = 302;

    //Emails of Support
    public static final String EmailSupport = "ceojaguarlabs@jaguarlabs.com";

    //function for encrypted the password of user.
    public static String encrypted(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    //Validate Email and password of user
    public static boolean Validation(String username, String password) {
        if ((username != null) && (password != null) && (password.length() > 5))
            return true;
        else
            return false;
    }

    public static void Message(String message) {
        Log.e("Error inesperado", message);
    }

    //Format Date
    public static String FormatDate(String startDate) {
        String date, year, mounth, day;
        if (startDate.length() != 0) {
            StringTokenizer stringTokenizer = new StringTokenizer(startDate, "-");
            year = stringTokenizer.nextToken();
            mounth = stringTokenizer.nextToken();
            day = stringTokenizer.nextToken();

            date = day + "/" + mounth + "/" + year;
            return date;
        }
        else
            return "";
    }
    public static String PresentSprint(String sprintOfRating) {
        StringTokenizer stringTokenizer = new StringTokenizer(sprintOfRating);
        return "SPRINT "+stringTokenizer.nextElement();
    }
}