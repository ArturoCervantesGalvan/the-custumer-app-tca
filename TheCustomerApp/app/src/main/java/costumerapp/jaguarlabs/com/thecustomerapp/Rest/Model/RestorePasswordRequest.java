package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

/**
 * Created by Arturo on 10/02/2016.
 */
public class RestorePasswordRequest extends BaseRequest {
    public String email;

    public RestorePasswordRequest(String request, String email){
        this.request = request;
        this.email = email;
    }
}
