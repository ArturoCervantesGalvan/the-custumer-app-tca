package costumerapp.jaguarlabs.com.thecustomerapp.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;

import costumerapp.jaguarlabs.com.thecustomerapp.Interface.ServicesWeb;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.DetailsProject;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.Technology;
import costumerapp.jaguarlabs.com.thecustomerapp.R;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.DetailsProjectRequest;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.DetailsProjectResponse;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Arturo on 13/01/2016.
 */
public class DetailsProjectFragment extends Fragment {
    public static final String requestDetailsProject = "detalle";
    public static final String comentsDetailsProject = "comentarios";
    public static final String scrumMasterContact = "contacto";
    public static final String nameScrumMaster = "nombre";
    public static final String rolSrumMaster = "rol";
    public static final String emailSrumMaster = "email";
    String idProject;
    //init Elements
    DetailsProject detailsProject;
    ArrayList<Technology> ListTechnology;
    //Construction of Object

    //On create Fragments
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //ListTechnology = getArguments().getParcelableArrayList("ListOfTechnology");
        //Util.Message("Se creo la lista dentro del Fragmento");
    }
    //On Create View
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Util.Message("LLego a crear la vista");
        //Create View of Details
        View view = inflater.inflate(R.layout.details_project, container, false);
        TextView tvAppNameDetails = (TextView) view.findViewById(R.id.tvAppNameDetails);
        TextView tvStartProjectDeatils = (TextView) view.findViewById(R.id.tvStartProjectDetails);
        TextView tvEndProjectDetails = (TextView) view.findViewById(R.id.tvEndProjectDetails);
        TextView tvScrummMastesName = (TextView) view.findViewById(R.id.tvScrummMasterName);
        TextView tvPositionScrummMaster = (TextView) view.findViewById(R.id.tvPositionScrummMaster);
        TextView tvEmailScrummMaster = (TextView) view.findViewById(R.id.tvEmailScrummMaster);
        TextView tvResumeSprint = (TextView) view.findViewById(R.id.tvResumenSprint);
        LinearLayout llAndroid = (LinearLayout) view.findViewById(R.id.llAndroid);
        LinearLayout llIos = (LinearLayout) view.findViewById(R.id.llIos);
        LinearLayout llWeb = (LinearLayout) view.findViewById(R.id.llWeb);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Util.baseUrl)
                .setClient(new OkClient(new OkHttpClient()))
                .build();
        ServicesWeb servicesWeb = restAdapter.create(ServicesWeb.class);
        DetailsProjectRequest detailsProjectRequest = new DetailsProjectRequest(DetailsProjectFragment.requestDetailsProject,idProject,Util.tokenUserLogin,Util.refreshTokenUserLogin);
        servicesWeb.getDetailsProject(detailsProjectRequest, new Callback<DetailsProjectResponse>() {
            @Override
            public void success(DetailsProjectResponse detailsProjectResponse, Response response) {
                
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
        //Data Project Details
        tvStartProjectDeatils.setText(detailsProject.getStartProject());
        tvAppNameDetails.setText(detailsProject.getNameProject());
        if ("".equals(detailsProject.getEndProject())) {
            tvEndProjectDetails.setText(R.string.coming_soon);
        } else {
            tvEndProjectDetails.setText(detailsProject.getEndProject());
        }
        //Data Scrumm Contact
        tvEmailScrummMaster.setText(detailsProject.getScrumMaster().getEmailScrumMaster());
        tvEmailScrummMaster.setOnClickListener(new SendEmail(tvEmailScrummMaster) {
            @Override
            public void onClick(View v) {
                super.onClick(v);
            }
        });
        tvPositionScrummMaster.setText(detailsProject.getScrumMaster().getRolScrumMaster());
        tvScrummMastesName.setText(detailsProject.getScrumMaster().getNamesScrumMaster());
        tvResumeSprint.setText(detailsProject.getComentsProject());
        //Add Technologies
        llAndroid.setVisibility(View.GONE);
        llIos.setVisibility(View.GONE);
        llWeb.setVisibility(View.GONE);
        for (int i = 0; i < ListTechnology.size(); i++) {
            Technology technology = ListTechnology.get(i);
            //Add Technology Android
            if ("Android".equals(technology.getTechnologyName())) {
                llAndroid.setVisibility(View.VISIBLE);
                //Add Data of the Technology
                TextView tvCurrentSprintAndroid = (TextView) view.findViewById(R.id.tvCurrentSprintAndroid);
                tvCurrentSprintAndroid.setText(technology.getSprint());
                TextView tvStartSprintAndroid = (TextView) view.findViewById(R.id.tvStartSprintAndroid);
                tvStartSprintAndroid.setText(technology.getStartSprint());
                TextView tvNextDeliveryAndroid = (TextView) view.findViewById(R.id.tvNextDeliveryAndroid);
                tvNextDeliveryAndroid.setText(technology.getNextDelivery());
                TextView tvTecnologyAndroid = (TextView) view.findViewById(R.id.tvTecnologyAndroid);
                tvTecnologyAndroid.setText(technology.getRestrictions());
            }
            //Add Technology Ios
            if ("Ios".equals(technology.getTechnologyName())) {
                llIos.setVisibility(View.VISIBLE);
                //Add Data of the Technology
                TextView tvCurrentSprintIos = (TextView) view.findViewById(R.id.tvCurrentSprintIOS);
                tvCurrentSprintIos.setText(technology.getSprint());
                TextView tvStartSprintIos = (TextView) view.findViewById(R.id.tvStartSprintIOS);
                tvStartSprintIos.setText(technology.getStartSprint());
                TextView tvNextDeliveryIos = (TextView) view.findViewById(R.id.tvNextDeliveryIOS);
                tvNextDeliveryIos.setText(technology.getNextDelivery());
                TextView tvTecnologyIos = (TextView) view.findViewById(R.id.tvTecnologyIOS);
                tvTecnologyIos.setText(technology.getRestrictions());
            }
            //Add Technology Web
            if ("Web".equals(technology.getTechnologyName())) {
                llWeb.setVisibility(View.VISIBLE);
                //Add Data of the Technology
                TextView tvCurrentSprintWeb = (TextView) view.findViewById(R.id.tvCurrentSprintWeb);
                tvCurrentSprintWeb.setText(technology.getSprint());
                TextView tvStartSprintWeb = (TextView) view.findViewById(R.id.tvStartSprintWeb);
                tvStartSprintWeb.setText(technology.getStartSprint());
                TextView tvNextDeliveryWeb = (TextView) view.findViewById(R.id.tvNextDeliveryWeb);
                tvNextDeliveryWeb.setText(technology.getNextDelivery());
                TextView tvTecnologyWeb = (TextView) view.findViewById(R.id.tvTecnologyWeb);
                tvTecnologyWeb.setText(technology.getRestrictions());
            }
        }
        return view;
    }
    private abstract class SendEmail implements View.OnClickListener{
        TextView textView;
        public SendEmail(TextView textView){
            this.textView = textView;
        }
        @Override
        public void onClick(View v) {
            String[] emails = new String[]{Util.EmailSupport, textView.getText().toString()};
            Util.Message("Mensaje al SrummMaster");
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, emails);
            intent.setType("message/rfc822");
            //intent.putExtra(Intent.EXTRA_CC,"arturocervantesits@gmail.com");
            startActivity(Intent.createChooser(intent, "Send mail..."));
        }
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }
}
