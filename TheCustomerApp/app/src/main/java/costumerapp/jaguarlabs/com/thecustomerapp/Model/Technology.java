package costumerapp.jaguarlabs.com.thecustomerapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Arturo on 13/01/2016.
 */
public class Technology implements Parcelable {

    String IdTechnology;
    String TechnologyName;
    String Sprint;
    String StartSprint;
    String NextDelivery;
    String Status;
    String Type;
    String Restrictions;
    //objet of Technology need of the any project
    public Technology() {

    }

    public String getIdTechnology() {
        return IdTechnology;
    }

    public void setIdTechnology(String idTechnology) {
        IdTechnology = idTechnology;
    }

    public String getTechnologyName() {
        return TechnologyName;
    }

    public void setTechnologyName(String technologyName) {
        TechnologyName = technologyName;
    }

    public String getSprint() {
        return Sprint;
    }

    public void setSprint(String sprint) {
        Sprint = sprint;
    }

    public String getStartSprint() {
        return StartSprint;
    }

    public void setStartSprint(String startSprint) {
        StartSprint = startSprint;
    }

    public String getNextDelivery() {
        return NextDelivery;
    }

    public void setNextDelivery(String nextDelivery) {
        NextDelivery = nextDelivery;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getRestrictions() {
        return Restrictions;
    }

    public void setRestrictions(String restrictions) {
        Restrictions = restrictions;
    }

    public Technology(Parcel in) {
        IdTechnology = in.readString();
        TechnologyName = in.readString();
        Sprint = in.readString();
        StartSprint = in.readString();
        NextDelivery = in.readString();
        Status = in.readString();
        Type = in.readString();
        Restrictions = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(IdTechnology);
        dest.writeString(TechnologyName);
        dest.writeString(Sprint);
        dest.writeString(StartSprint);
        dest.writeString(NextDelivery);
        dest.writeString(Status);
        dest.writeString(Type);
        dest.writeString(Restrictions);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Technology> CREATOR = new Parcelable.Creator<Technology>() {
        @Override
        public Technology createFromParcel(Parcel in) {
            return new Technology(in);
        }

        @Override
        public Technology[] newArray(int size) {
            return new Technology[size];
        }
    };
}
