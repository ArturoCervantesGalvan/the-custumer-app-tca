package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import costumerapp.jaguarlabs.com.thecustomerapp.Model.Project;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 11/02/2016.
 */
public class ListProjectResponse extends BaseResponse {
    @SerializedName(Util.projects)
    ArrayList<Project> listProyects;

    public ArrayList<Project> getListProyects() {
        return listProyects;
    }

    public void setListProyects(ArrayList<Project> listProyects) {
        this.listProyects = listProyects;
    }
}
