package costumerapp.jaguarlabs.com.thecustomerapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventRestorePassword;
import costumerapp.jaguarlabs.com.thecustomerapp.MainActivity;
import costumerapp.jaguarlabs.com.thecustomerapp.R;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 12/01/2016.
 */
public class RestorePasswordFragment extends Fragment {
    public EventRestorePassword eventRestorePassword;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        //Create View
        if(eventRestorePassword.getCode() == 200){
            //Create view of answer Affirm
            view = inflater.inflate(R.layout.confirm_send_restore_password,container,false);
            TextView EmailRestoreEmail = (TextView) view.findViewById(R.id.tvEmailSendRestore);
            EmailRestoreEmail.setText(eventRestorePassword.getEmail());
            TextView tvConfirm = (TextView) view.findViewById(R.id.tvConfirm);
            tvConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListenerRestorePassword)getActivity()
                            .getSupportFragmentManager()
                            .findFragmentByTag(MainActivity.loginFragment))
                            .listenerRestoreEvent(Util.confirm);
                }
            });
        }else{
            //create View of Bad Request
            view = inflater.inflate(R.layout.error_confirmid_restore_password,container,false);
            TextView EmailRestoreEmail = (TextView) view.findViewById(R.id.tvEmailSendRestore);
            EmailRestoreEmail.setText(eventRestorePassword.getEmail());
            TextView tvRetry = (TextView) view.findViewById(R.id.tvRetry);
            tvRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListenerRestorePassword) getActivity()
                            .getSupportFragmentManager()
                            .findFragmentByTag(MainActivity.loginFragment))
                            .listenerRestoreEvent(Util.retry);
                }
            });
        }

        return view;
    }

    public void setEventRestorePassword(EventRestorePassword eventRestorePassword) {
        this.eventRestorePassword = eventRestorePassword;
    }
    public interface  ListenerRestorePassword{
        void listenerRestoreEvent(String actionOfFragment);
    }
}
