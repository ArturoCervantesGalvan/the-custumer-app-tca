package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

/**
 * Created by Arturo on 12/02/2016.
 */
public class BaseTokenRequest extends BaseRequest {
    public String tokenUserLogin;
    public String refreshTokenUserLogin;

    public String getTokenUserLogin() {
        return tokenUserLogin;
    }

    public void setTokenUserLogin(String tokenUserLogin) {
        this.tokenUserLogin = tokenUserLogin;
    }

    public String getRefreshTokenUserLogin() {
        return refreshTokenUserLogin;
    }

    public void setRefreshTokenUserLogin(String refreshTokenUserLogin) {
        this.refreshTokenUserLogin = refreshTokenUserLogin;
    }
}
