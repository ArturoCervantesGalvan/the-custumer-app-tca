package costumerapp.jaguarlabs.com.thecustomerapp.Model;

import com.google.gson.annotations.SerializedName;

import costumerapp.jaguarlabs.com.thecustomerapp.Fragments.LoginFragment;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 11/01/2016.
 */
public class User {
    @SerializedName(Util.idUser)
    String idUser;
    @SerializedName(LoginFragment.idCustumer)
    String idCostumer;
    @SerializedName(Util.token)
    String token;
    @SerializedName(Util.refreshToken)
    String refreshToken;
    @SerializedName(LoginFragment.fullNameUser)
    String fullUsername;
    //Objet of user and load data

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdCostumer() {
        return idCostumer;
    }

    public void setIdCostumer(String idCostumer) {
        this.idCostumer = idCostumer;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getFullUsername() {
        return fullUsername;
    }

    public void setFullUsername(String fullUsername) {
        this.fullUsername = fullUsername;
    }
}
