package costumerapp.jaguarlabs.com.thecustomerapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.squareup.okhttp.OkHttpClient;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import costumerapp.jaguarlabs.com.thecustomerapp.Adapter.AdapterProject;
import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventErrorServer;
import costumerapp.jaguarlabs.com.thecustomerapp.Interface.ServicesWeb;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.Project;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.User;
import costumerapp.jaguarlabs.com.thecustomerapp.R;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.ListProjectRequest;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.ListProjectResponse;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Arturo on 13/01/2016.
 */
public class ListProjectFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    //init
    User user;
    ArrayList<Project> listProject;
    SwipeRefreshLayout swipeRefreshLayout;
    AdapterProject adapterProject;
    ListView lvListProject;
    public static final String requestListProject = "listado";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_project,container,false);
        //Load List View
        lvListProject = (ListView) view.findViewById(R.id.lvListProject);
        lvListProject.setVisibility(View.GONE);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Util.baseUrl)
                .setClient(new OkClient(new OkHttpClient()))
                .build();
        ServicesWeb servicesWeb = restAdapter.create(ServicesWeb.class);
        ListProjectRequest listProjectRequest = new ListProjectRequest(ListProjectFragment.requestListProject,user.getIdUser());
        servicesWeb.getListProject(listProjectRequest, new Callback<ListProjectResponse>() {
            @Override
            public void success(ListProjectResponse listProjectResponse, Response response) {
                if (response.getStatus() == 200) {
                    switch (listProjectResponse.getCode()) {
                        //Request Success
                        case Util.codeRequestAfirm:
                            Util.Message("Code Accepts");
                            listProject = listProjectResponse.getListProyects();
                            Util.Message("Adaptador con exito LP");
                            adapterProject = new AdapterProject(getActivity().getApplicationContext(), listProject);
                            Util.Message("Se creo con exito la vista LP");
                            lvListProject.setAdapter(adapterProject);
                            getActivity().getSupportFragmentManager().popBackStack();
                            getActivity().getSupportFragmentManager().executePendingTransactions();
                            lvListProject.setVisibility(View.VISIBLE);
                            break;
                        //Another Result
                        case Util.codeBadRequest:
                        case Util.codeInsufficientData:
                        default:
                            EventErrorServer eventErrorServer = new EventErrorServer(listProjectResponse.getMessage());
                            EventBus.getDefault().post(eventErrorServer);
                            break;
                    }
                }
            }
            @Override
            public void failure(RetrofitError error) {
                EventErrorServer eventErrorServer = new EventErrorServer(error.getMessage());
                EventBus.getDefault().post(eventErrorServer);
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swlRefreshList);
        swipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) this);
        lvListProject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Action for Details of Project
                Project project = (Project) parent.getItemAtPosition(position);
                DetailsProjectFragment detailsProjectFragment = new DetailsProjectFragment();
                detailsProjectFragment.setIdProject(project.getIdProject());

                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fgMain, new LoaderFragment(), Util.loaderFragments)
                        .addToBackStack(null)
                        .commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();
                //((MainActivity) getActivity()).RequestDeatilsProject(project.getIdProject());
            }
        });
        return view;
    }
    //Send data of the Refresh data
    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Util.baseUrl)
                .setClient(new OkClient(new OkHttpClient()))
                .build();
        ServicesWeb servicesWeb = restAdapter.create(ServicesWeb.class);
        ListProjectRequest listProjectRequest = new ListProjectRequest(ListProjectFragment.requestListProject,user.getIdUser());
        servicesWeb.getListProject(listProjectRequest, new Callback<ListProjectResponse>() {
            @Override
            public void success(ListProjectResponse listProjectResponse, Response response) {
                if (response.getStatus() == 200) {
                    switch (listProjectResponse.getCode()) {
                        //Request Success
                        case Util.codeRequestAfirm:
                            Util.Message("Code Accepts");
                            listProject = listProjectResponse.getListProyects();
                            Util.Message("Adaptador con exito LP");
                            adapterProject.notifyDataSetChanged();
                            Util.Message("Se creo con exito la vista LP");
                            swipeRefreshLayout.setRefreshing(false);
                            break;
                        //Another Result
                        case Util.codeBadRequest:
                        case Util.codeInsufficientData:
                        default:
                            EventErrorServer eventErrorServer = new EventErrorServer(listProjectResponse.getMessage());
                            EventBus.getDefault().post(eventErrorServer);
                            break;
                    }
                }

            }

            @Override
            public void failure(RetrofitError error) {
                EventErrorServer eventErrorServer = new EventErrorServer(error.getMessage());
                EventBus.getDefault().post(eventErrorServer);
            }
        });
    }

    public void RefreshList(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        JSONArray jsonArrayProject = jsonObject.getJSONArray("proyectos");
        Util.Message("LLego el JSon para el Arrya List LP");
        //Begin tour of JSONArray
        if (jsonArrayProject.length() != 0) {
            Util.Message("Comienza el Recorrido LP");
            if (listProject.size() != 0){
                listProject.clear();
                Util.Message("Lista vacia LP");
            }
            for (int i = 0; i < jsonArrayProject.length(); i++) {
                //Load Objet in the Array List
                Project project = new Project();
                JSONObject jsonProject = jsonArrayProject.getJSONObject(i);
                project.setIdProject(jsonProject.getString("id_proyecto"));
                project.setNameProject(jsonProject.getString("nombre"));
                project.setStartDate(Util.FormatDate(jsonProject.getString("fecha_inicio")));
                project.setEndDate(Util.FormatDate(jsonProject.getString("fecha_finalizacion")));
                project.setUrlImagen(jsonProject.getString("imagen"));
                listProject.add(project);
            }//End tour of JSonArray
            Util.Message("Finalizo el recorrido LP");
            adapterProject.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
        }
    }


    public void setUser(User user) {
        this.user = user;
    }
}
