package costumerapp.jaguarlabs.com.thecustomerapp;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 25/01/2016.
 */
public class ConnectionServiceAndroid extends IntentService {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();

    public ConnectionServiceAndroid(){
        super("My service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //Create Body petition
        RequestBody body = RequestBody.create(JSON,intent.getStringExtra("parameters"));
        //Create Request
        Request request = new Request.Builder()
                .url(intent.getStringExtra("url"))
                .post(body)
                .build();
        try {
            //init petition
            Response response = client.newCall(request).execute();
            //Result of the petition
            Intent intentResult = new Intent();
            //
            intentResult.putExtra("json", response.body().string());
            intentResult.setAction(intent.getStringExtra("action"));
            Util.Message("Se manda intent de respuesta");
            //If exist 2nd petition, Send Petition
            if(intent.getStringExtra("2ndParameters")!=null){
                body = RequestBody.create(JSON,intent.getStringExtra("2ndParameters"));
                request = new Request.Builder()
                        .url(intent.getStringExtra("url"))
                        .post(body)
                        .build();
                response = client.newCall(request).execute();
                intentResult.putExtra("2ndJson",response.body().string());
            }
            //Load Intent result
            LocalBroadcastManager.getInstance(this).sendBroadcast(intentResult);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
