package costumerapp.jaguarlabs.com.thecustomerapp;

import android.os.AsyncTask;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import costumerapp.jaguarlabs.com.thecustomerapp.Model.Petition;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.ResultPetition;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 11/01/2016.
 */
public class ConnectionServiceWeb extends AsyncTask<Petition, Integer, ResultPetition> {
    //Global
    MainActivity main;
    OkHttpClient client = new OkHttpClient();
    JSONObject resultOfPetition;
    ResultPetition resultPetition = new ResultPetition();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public ConnectionServiceWeb(MainActivity main){
        this.main = main;
    }
    //Service Web implements
    @Override
    protected ResultPetition doInBackground(Petition... params) {
        RequestBody body = RequestBody.create(JSON, params[0].getParameters());
        //Load Request with data of petition
        Request request = new Request.Builder()
                    .url(params[0].getUrl())
                    .post(body)
                    .build();
        try {
            //Response
            Response response = client.newCall(request).execute();
            Util.Message("Send to date");
            try {
                resultOfPetition = new JSONObject(response.body().string());
                Util.Message("Create JsonObject");
                resultPetition.setJsonObject(resultOfPetition);
                resultPetition.setCodePetition(params[0].getCodePetition());
                if (params[0].getCodePetition() == Util.CodePetitionListQualify) {
                    resultPetition.setAppName(params[0].getAppName());
                }
                //return resultPetition;
            } catch (JSONException e) {
                Util.Message("Error create JsonObjet");
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(params[0].getParameters2nd()!=null){
            body = RequestBody.create(JSON, params[0].getParameters2nd());
            //Load Request with data of petition
            request = new Request.Builder()
                .url(params[0].getUrl())
                .post(body)
                .build();
            try {
                //Response
                Response response = client.newCall(request).execute();
                Util.Message("Send to date");
                try {
                    resultOfPetition = new JSONObject(response.body().string());
                    Util.Message("Create JsonObject");
                    resultPetition.setJsonObject2ndPetition(resultOfPetition);
                    //resultPetition.setCodePetition(params[0].getCodePetition());
                    /*if (params[0].getCodePetition() == Util.CodePetitionListQualify) {
                        resultPetition.setAppName(params[0].getAppName());
                    }*/
                    return resultPetition;
                } catch (JSONException e) {
                    Util.Message("Error create JsonObjet");
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            return resultPetition;
        }
        Util.Message("LLego aquí");
        return resultPetition;
    }

    @Override
    protected void onPostExecute(ResultPetition jsonObject) {
        super.onPostExecute(jsonObject);
        Util.Message("PostExecute");
        /*try {
            //Return of the data.
            Util.Message("Start Funtion Main");
            main.EvaluationJSON(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    //Cancel Process
    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
