package costumerapp.jaguarlabs.com.thecustomerapp.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import costumerapp.jaguarlabs.com.thecustomerapp.Model.Project;
import costumerapp.jaguarlabs.com.thecustomerapp.R;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 13/01/2016.
 */
public class AdapterProject extends BaseAdapter {
    Context context;
    ArrayList<Project> listProject;
    //Activity mActivity;
    public AdapterProject(Context applicationContext, ArrayList<Project> listProject) {
        this.context = applicationContext;
        this.listProject = listProject;
        //this.mActivity = activity;
    }

    @Override
    public int getCount() {
        return listProject.size();
    }

    @Override
    public Object getItem(int position) {
        return listProject.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, final ViewGroup parent) {
        //create view each Project in the list
        final Project project = (Project) getItem(position);
        if(view == null){
            view = View.inflate(context,R.layout.row_list_project,null);
        }
        //Textview for each row
        TextView tvAppNameList = (TextView) view.findViewById(R.id.tvAppNameList);
        TextView tvStartProjectListUtil = (TextView) view.findViewById(R.id.tvStartProjectUtil);
        TextView tvStartProjectList = (TextView) view.findViewById(R.id.tvStartProjectList);
        TextView tvEndProjectListUtil = (TextView) view.findViewById(R.id.tvEndProjectUtil);
        TextView tvEndProjectList = (TextView) view.findViewById(R.id.tvEndProjectList);

        //Style of each TextView
        tvAppNameList.setTextAppearance(context,R.style.TextSmallBlackApp);
        tvStartProjectListUtil.setTextAppearance(context,R.style.TextSmallBlack);
        tvStartProjectList.setTextAppearance(context,R.style.TextSmallGray);
        tvEndProjectListUtil.setTextAppearance(context,R.style.TextSmallBlack);
        tvEndProjectList.setTextAppearance(context,R.style.TextSmallGray);

        //Load Image
        ImageView ivAppImgList = (ImageView) view.findViewById(R.id.ivAppImgList);
        tvAppNameList.setText(project.getNameProject());
        //Add data of the Project in the list
        if(project.getStartDate().length()==0)
            tvStartProjectList.setText(R.string.coming_soon);
        else {
            tvStartProjectList.setText(project.getStartDate());
        }
        if(project.getEndDate().length()==0)
            tvEndProjectList.setText(R.string.coming_soon);
        else
            tvEndProjectList.setText(project.getEndDate());
        if("URL".equals(project.getUrlImagen()))
            Util.Message("App whout Imagen");
        else
            Util.Message("App whit Imagen");
        ImageView ivDetailsProject = (ImageView) view.findViewById(R.id.ivDetailsProject);
        //Action of click in icon of Details
        ivDetailsProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.Message("For View Details project " + project.getNameProject());
                //Function of detaills the project
                //MainActivity.RequestDeatilsProject(project.getIdProject());
                //((MainActivity)mActivity).RequestDeatilsProject(project.getIdProject());
            }
        });
        //Action of click in icon Qualify
        ImageView ivQualifyProject = (ImageView) view.findViewById(R.id.ivQualifyProject);
        ivQualifyProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.Message("For Qualify the project "+project.getNameProject());
                //((MainActivity)mActivity).RequestQualifyProject(project.getIdProject(),project.getNameProject());
                //Funtion of Qualify Projectg
            }
        });
        return view;
    }

}
