package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

import com.google.gson.annotations.SerializedName;

import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 10/02/2016.
 */
public class BaseResponse {
    @SerializedName(Util.code)
    public String code;

    @SerializedName(Util.message)
    public String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
