package costumerapp.jaguarlabs.com.thecustomerapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import costumerapp.jaguarlabs.com.thecustomerapp.MainActivity;
import costumerapp.jaguarlabs.com.thecustomerapp.R;

/**
 * Created by Arturo on 15/01/2016.
 */
public class LogoutFragment extends Fragment {
    String Email;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Set Arguments
        super.onCreate(savedInstanceState);
        Email = getArguments().getString("Email");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Init of the View
        View view = inflater.inflate(R.layout.logout,container,false);
        TextView tvUserLogout = (TextView) view.findViewById(R.id.tvEmailLogout);
        tvUserLogout.setText(Email);
        TextView tvCancelLogout = (TextView) view.findViewById(R.id.tvCancelLogout);
        //Function of the Cancel Logout...
        tvCancelLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        TextView tvConfirmLogout = (TextView) view.findViewById(R.id.tvConfirmLogout);
        //Function of the Confirm Logout ...
        tvConfirmLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Comeback to Login...
                ((MainActivity)getActivity()).ShowLogin();
            }
        });
        return view;
    }
}
