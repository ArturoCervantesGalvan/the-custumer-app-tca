package costumerapp.jaguarlabs.com.thecustomerapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import costumerapp.jaguarlabs.com.thecustomerapp.Adapter.AdapterOfRating;
import costumerapp.jaguarlabs.com.thecustomerapp.MainActivity;
import costumerapp.jaguarlabs.com.thecustomerapp.Model.OfRating;
import costumerapp.jaguarlabs.com.thecustomerapp.R;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 18/01/2016.
 */
public class QualifyFragment extends Fragment implements AdapterOfRating.ListenerAdapterOfRatingForQualifyFrg {
    //List Qualific
    ArrayList<OfRating> ListRatingAndroid = new ArrayList<>();
    ArrayList<OfRating> ListRatingIOS = new ArrayList<>();
    ArrayList<OfRating> ListRatingWEB = new ArrayList<>();
    ArrayList<String> TechnologyOfRating = new ArrayList<>();
    ArrayList<ArrayList<OfRating>> ListOfRating = new ArrayList<ArrayList<OfRating>>();

    //History Qualific
    ArrayList<OfRating> ListRatingHistoryAndroid = new ArrayList<>();
    ArrayList<OfRating> ListRatingHistoryIOS = new ArrayList<>();
    ArrayList<OfRating> ListRatingHistoryWEB = new ArrayList<>();
    ArrayList<String> TechnologyOfRatingHistory = new ArrayList<>();
    ArrayList<ArrayList<OfRating>> ListOfRatingHistory = new ArrayList<ArrayList<OfRating>>();
    OfRating ofRatingEmpty = new OfRating();
    static AdapterOfRating adapterOfRating;
    static AdapterOfRating adapterOfRatingHistory;
    TabHost tabHost;

    //init of the Fragment
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Util.Message("***Entramos en Qualify Fragment***");
        super.onCreate(savedInstanceState);
        //Validation of the data
        if(ListOfRatingHistory!=null){
            ListOfRatingHistory.clear();
            Util.Message("Borramos los existentes de historial");
            //Util.Message(String.valueOf(TechnologyOfRating.size()));
        }
        if(ListOfRating!=null){
            ListOfRating.clear();
            Util.Message("Borramos los existentes por calificar");
        }
        if(TechnologyOfRatingHistory!=null){
            TechnologyOfRatingHistory.clear();
            Util.Message("Borramos las technologias por calificar historial");
        }
        if(TechnologyOfRating!=null){
            TechnologyOfRating.clear();
            Util.Message("Borramos las technologias por calificar");
        }

        //Set List Qualify
        if(getArguments().getParcelableArrayList("ListOfRatingIOS")!=null) {
            ListRatingIOS = getArguments().getParcelableArrayList("ListOfRatingIOS");
            ListOfRating.add(ListRatingIOS);
            Util.Message("LLegaron el argumento de IOS qualify");
        }
        if(getArguments().getParcelableArrayList("ListOfRatingAndroid")!=null) {
            ListRatingAndroid = getArguments().getParcelableArrayList("ListOfRatingAndroid");
            Util.Message("LLegaron el argumento de Android qualify");
            ListOfRating.add(ListRatingAndroid);
        }
        if(getArguments().getParcelableArrayList("ListOfRatingWeb")!=null) {
            ListRatingWEB = getArguments().getParcelableArrayList("ListOfRatingWeb");
            Util.Message("LLegaron el argumento de Web qualify");
            ListOfRating.add(ListRatingWEB);
        }
        if(getArguments().getStringArrayList("TechnologyOfRating")!=null) {
            TechnologyOfRating = getArguments().getStringArrayList("TechnologyOfRating");
            Util.Message(String.valueOf(TechnologyOfRating.size()));
            Util.Message("LLegaron el argumento de Tecnologias qualify");
        }
        //Set History Qualify
        if(getArguments().getParcelableArrayList("ListOfRatingHistoryIOS")!=null) {
            ListRatingHistoryIOS = getArguments().getParcelableArrayList("ListOfRatingHistoryIOS");
            ListOfRatingHistory.add(ListRatingHistoryIOS);
            Util.Message("LLegaron el argumento de IOS qualify historial");
        }
        if(getArguments().getParcelableArrayList("ListOfRatingHistoryAndroid")!=null) {
            ListRatingHistoryAndroid = getArguments().getParcelableArrayList("ListOfRatingHistoryAndroid");
            Util.Message("LLegaron el argumento de Android qualify historial");
            ListOfRatingHistory.add(ListRatingHistoryAndroid);
        }
        if(getArguments().getParcelableArrayList("ListOfRatingHistoryWeb")!=null) {
            ListRatingHistoryWEB = getArguments().getParcelableArrayList("ListOfRatingHistoryWeb");
            Util.Message("LLegaron el argumento de Web qualify historial");
            ListOfRatingHistory.add(ListRatingHistoryWEB);
        }
        if(getArguments().getStringArrayList("TechnologyOfRatingHistory")!=null) {
            TechnologyOfRatingHistory = getArguments().getStringArrayList("TechnologyOfRatingHistory");
            Util.Message("LLegaron el argumento de Tecnologias qualify historial");
        }
        ValidationQualifyAndHistory(TechnologyOfRating,TechnologyOfRatingHistory);
        Util.Message("Las tecnologias a calificar");
        Util.Message(String.valueOf(TechnologyOfRating.size()));
        Util.Message("Las tecnologias colecciones a calificar");
        Util.Message(String.valueOf(ListOfRating.size()));
        Util.Message("Las tecnologias en historial");
        Util.Message(String.valueOf(TechnologyOfRatingHistory.size()));
        Util.Message("Las tecnologias colecciones en historial");
        Util.Message(String.valueOf(ListOfRatingHistory.size()));
        //Validation Qualify and History List

    }

    private void ValidationQualifyAndHistory(ArrayList<String> technologyOfRating, ArrayList<String> technologyOfRatingHistory) {
        //Add Android in Qualify if in History contains
        if((!(technologyOfRating.contains("Android"))) && technologyOfRatingHistory.contains("Android") ) {
            TechnologyOfRating.add("Android");
            ListRatingAndroid.add(ofRatingEmpty);
            ListOfRating.add(ListRatingAndroid);
        }
        //Add IOS in Qualify if in History contains
        if((!(technologyOfRating.contains("IOS"))) && technologyOfRatingHistory.contains("IOS")) {
            TechnologyOfRating.add("IOS");
            ListRatingIOS.add(ofRatingEmpty);
            ListOfRating.add(ListRatingIOS);
        }
        //Add WEB in Qualify if in History contains
        if((!(technologyOfRating.contains("Web"))) && technologyOfRatingHistory.contains("Web")) {
            TechnologyOfRating.add("Web");
            ListRatingWEB.add(ofRatingEmpty);
            ListOfRating.add(ListRatingWEB);
        }
        //Agregado
        //Add Android in History if in Qualify contains
        if((!(technologyOfRatingHistory.contains("Android"))) && technologyOfRating.contains("Android") ) {
            TechnologyOfRatingHistory.add("Android");
            ListRatingHistoryAndroid.add(ofRatingEmpty);
            ListOfRatingHistory.add(ListRatingHistoryAndroid);
        }
        //Add IOS in History if in Qualify contains
        if((!(technologyOfRatingHistory.contains("IOS"))) && technologyOfRating.contains("IOS")) {
            TechnologyOfRatingHistory.add("IOS");
            ListRatingHistoryIOS.add(ofRatingEmpty);
            ListOfRatingHistory.add(ListRatingHistoryIOS);
        }
        //Add Web in History if in Qualify contains
        if((!(technologyOfRatingHistory.contains("Web"))) && technologyOfRating.contains("Web")) {
            Util.Message("Llega y se agrega web");
            TechnologyOfRatingHistory.add("Web");
            ListRatingHistoryWEB.add(ofRatingEmpty);
            ListOfRatingHistory.add(ListRatingHistoryWEB);
        }
        //fin de agregado
    }
    //Create View
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qualify, container, false);
        tabHost = (TabHost) view.findViewById(R.id.tabHost);
        tabHost.setup();
        TabHost.TabSpec spec = tabHost.newTabSpec("Calificar");
        spec.setContent(R.id.llQualifyAction);
        //add Tab Qualify
        spec.setIndicator(getResources().getString(R.string.qualify));
        //tabHost.getTabWidget().getChildAt(0).setBackgroundColor(getResources().getColor(R.color.ORAGEN));
        tabHost.addTab(spec);
        spec = tabHost.newTabSpec("Historial");
        //add Tab History
        spec.setIndicator(getResources().getString(R.string.history));
        spec.setContent(R.id.llHistoryQualify);
        tabHost.addTab(spec);
        //tabHost.getTabWidget().getChildAt(1).setBackgroundColor(getResources().getColor(R.color.ORAGEN));
        //Active Qualify tab
        tabHost.setCurrentTab(0);
        tabHost.getTabWidget().getChildAt(0).setBackgroundColor(getResources().getColor(R.color.WHITE));

        //Function of the change of background in tabs
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
                    tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getResources().getColor(R.color.ORAGEN)); // unselected
                    TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
                    tv.setTextColor(getResources().getColor(R.color.WHITE));
                }
                tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(getResources().getColor(R.color.WHITE)); // selected
                TextView tv = (TextView) tabHost.getCurrentTabView().findViewById(android.R.id.title); //for Selected Tab
                tv.setTextColor(getResources().getColor(R.color.BLACK));
            }
        });
        //Validation of the list for adapter
        if(ValidationArrayList(TechnologyOfRating,ListOfRating)){
            ExpandableListView expandableListView = (ExpandableListView) view.findViewById(R.id.elvTechnology);
            View header = inflater.inflate(R.layout.hader_expandable_list,null,false);
            adapterOfRating = new AdapterOfRating((MainActivity)getActivity(),this ,TechnologyOfRating,ListOfRating,true);
            expandableListView.addHeaderView(header);
            expandableListView.setAdapter(adapterOfRating);
        }
        //Validation of the List for adapter
        if(ValidationArrayList(TechnologyOfRatingHistory,ListOfRatingHistory)){
            ExpandableListView expandableListViewHistory = (ExpandableListView) view.findViewById(R.id.elvHistoryQualify);
            View header = inflater.inflate(R.layout.hader_expandable_list,null,false);
            TextView tvInstructionOfQualify = (TextView) header.findViewById(R.id.tvInstructionQualify);
            tvInstructionOfQualify.setVisibility(View.GONE);
            adapterOfRatingHistory = new AdapterOfRating((MainActivity)getActivity(),this ,TechnologyOfRatingHistory,ListOfRatingHistory,false);
            expandableListViewHistory.addHeaderView(header);
            expandableListViewHistory.setAdapter(adapterOfRatingHistory);
        }
        return view;
    }
    //Validation of the ArrayList
    private boolean ValidationArrayList( ArrayList<String> TechnologyOfRatingHistory, ArrayList<ArrayList<OfRating>> ListOfRatingHistory) {
        return (TechnologyOfRatingHistory != null) && (ListOfRatingHistory != null);
    }

    //Remove, order and uploads of the Rations
    public void RemoveOfRating(ArrayList<OfRating> Original, ArrayList<OfRating> news, OfRating ofRating){
        Original.remove(ofRating);
        ListOfRatingHistory.remove(news);
        //agregado
        if(news.size()==1){
            if(news.get(0).getIdOfRating()!=null)
                news.clear();
            else
                news.add(ofRating);
        }
        else
            news.add(ofRating);
        //fin de agregado
        Collections.sort(news, new Comparator<OfRating>() {
            @Override
            public int compare(OfRating lhs, OfRating rhs) {
                return Integer.valueOf(lhs.getOrderOfQuality()).compareTo(rhs.getOrderOfQuality());
            }
        });
        ListOfRatingHistory.add(news);
    }

    @Override
    public void AddSprintQualify(OfRating ofRating) {
        Util.Message("LLegamos a refrescar el adapter");
        //Selections of the Collections
        switch (ofRating.getTechnologyOfRating()){
            case "Android":
                /*ListOfRatingHistory.remove(ListRatingHistoryAndroid);
                ListRatingHistoryAndroid.add(ofRating);
                ListOfRatingHistory.add(ListRatingHistoryAndroid);*/
                RemoveOfRating(ListRatingAndroid, ListRatingHistoryAndroid, ofRating);
                //Removido
                /*
                if(!TechnologyOfRatingHistory.contains("Android"))
                    TechnologyOfRatingHistory.add("Android");*/
                //fin removido
                if(ListRatingAndroid.isEmpty())
                    ListRatingAndroid.add(ofRatingEmpty);
                Util.Message("Se refresco Android");
                adapterOfRating.notifyDataSetChanged();
                adapterOfRatingHistory.notifyDataSetChanged();
                break;
            case "iOS":
                /*ListOfRatingHistory.remove(ListRatingHistoryIOS);
                ListRatingHistoryIOS.add(ofRating);
                ListOfRatingHistory.add(ListRatingHistoryIOS);*/
                RemoveOfRating(ListRatingIOS,ListRatingHistoryIOS,ofRating);
                if(ListRatingIOS.isEmpty())
                    ListRatingIOS.add(ofRatingEmpty);
                Util.Message("Se refresco IOS");
                //Removido
                /*
                if(!TechnologyOfRatingHistory.contains("IOS"))
                    TechnologyOfRatingHistory.add("IOS");*/
                //fin removido
                adapterOfRating.notifyDataSetChanged();
                adapterOfRatingHistory.notifyDataSetChanged();
                break;
            case "Web":
                /*ListOfRatingHistory.remove(ListRatingHistoryWEB);
                ListRatingHistoryWEB.add(ofRating);
                ListOfRatingHistory.add(ListRatingHistoryWEB);*/
                RemoveOfRating(ListRatingWEB,ListRatingHistoryWEB,ofRating);
                if(ListRatingWEB.isEmpty())
                    ListRatingWEB.add(ofRatingEmpty);
                Util.Message("Se refresco WEB");
                //Removido
                /*if(!TechnologyOfRatingHistory.contains("Web"))
                    TechnologyOfRatingHistory.add("Web");*/
                //Fin Removido
                adapterOfRating.notifyDataSetChanged();
                adapterOfRatingHistory.notifyDataSetChanged();
                break;
            default:
                Util.Message("Unexpected Error");
                Util.Message(ofRating.getTechnologyOfRating());
                break;
        }
    }
}
