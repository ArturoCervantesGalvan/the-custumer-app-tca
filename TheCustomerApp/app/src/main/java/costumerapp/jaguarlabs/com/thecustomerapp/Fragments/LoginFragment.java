package costumerapp.jaguarlabs.com.thecustomerapp.Fragments;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventErrorServer;
import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventLoginSuccess;
import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventRestorePassword;
import costumerapp.jaguarlabs.com.thecustomerapp.Interface.ServicesWeb;
import costumerapp.jaguarlabs.com.thecustomerapp.R;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.UserRequest;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.UserResponse;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Arturo on 06/01/2016.
 */
public class LoginFragment extends Fragment implements RestorePasswordFragment.ListenerRestorePassword, ForgotPasswordFragment.ListenerDestroyForgotPassword {
    View view;
    ImageView ivClearEmail;
    EditText etUsername;
    EditText etPassword;
    RestorePasswordFragment restorePasswordFragment;

    public static final String user= "usuario";
    public static final String idCustumer = "id_cliente";
    public static final String fullNameUser = "nombre_completo_usuario";
    public static final String fragmentForgotPassword = "ForgotPassword";
    public static final String fragmentRestorePassword = "RestorePassword";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        restorePasswordFragment = new RestorePasswordFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //create View
        view = inflater.inflate(R.layout.login,container,false);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        ivClearEmail = (ImageView) view.findViewById(R.id.ivClearEmail);
        ivClearEmail.setVisibility(View.INVISIBLE);
        etUsername = (EditText) view.findViewById(R.id.etEmail);
        //Add listener of the input text
        etUsername.addTextChangedListener(etUsernameWatcher);
        TextView tvForgotPassword = (TextView) view.findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setPaintFlags(tvForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.Message("Click olvide mi contraseña");
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fgMain,new ForgotPasswordFragment(),LoginFragment.fragmentForgotPassword)
                        .addToBackStack(null)
                        .commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();
                etUsername.setVisibility(View.GONE);
                etPassword.setVisibility(View.GONE);
            }
        });
        TextView tvLogin = (TextView) view.findViewById(R.id.tvLogin);
        tvLogin.setOnClickListener(new CustomToast(inflater) {
            @Override
            public void onClick(View v) {
                super.onClick(v);
            }
        });
        //Action of Clean Email Text Edit
        ivClearEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etUsername.setText("");
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    //Function of change state of Clear text Email
    private final TextWatcher etUsernameWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            ivClearEmail.setVisibility(View.VISIBLE);
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length() == 0)
                ivClearEmail.setVisibility(View.INVISIBLE);
            else
                ivClearEmail.setVisibility(View.VISIBLE);
        }
    };

    @Override
    public void listenerRestoreEvent(String actionOfFragment) {
        switch (actionOfFragment){
            case Util.confirm:
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().getSupportFragmentManager().executePendingTransactions();
                break;
            case Util.retry:
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .show(getActivity().getSupportFragmentManager().findFragmentByTag(LoginFragment.fragmentForgotPassword))
                        .commit();
                getActivity().getSupportFragmentManager().executePendingTransactions();
                break;
            case Util.canceled:
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().getSupportFragmentManager().executePendingTransactions();
                break;
        }
    }

    @Override
    public void destroyForgotPassword(boolean b) {
        if(b){
            etUsername.setVisibility(View.VISIBLE);
            etPassword.setVisibility(View.VISIBLE);
        }
    }


    private abstract class CustomToast implements View.OnClickListener{
        LayoutInflater inflater;
        CustomToast(LayoutInflater inflater){
            this.inflater= inflater;
        }
        //Function of Click
        @Override
        public void onClick(View v) {
            if (Util.Validation(etUsername.getText().toString(), etPassword.getText().toString())) {
                //Start inico de services
                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(Util.baseUrl)
                        .setClient(new OkClient(new OkHttpClient()))
                        .build();

                ServicesWeb servicesWeb = restAdapter.create(ServicesWeb.class);
                UserRequest userRequest = new UserRequest();
                userRequest.setLogin(etUsername.getText().toString());
                userRequest.setPassword(Util.encrypted(etPassword.getText().toString()));
                servicesWeb.getUser(userRequest, new Callback<UserResponse>() {
                    @Override
                    public void success(UserResponse userResponse, Response response) {

                        //Validation of the Status Server
                        if(response.getStatus()==200) {
                            switch (userResponse.getCode()){
                                case Util.codeRequestAfirm:
                                    EventLoginSuccess event = new EventLoginSuccess(userResponse.getUser());
                                    EventBus.getDefault().post(event);
                                    break;
                                case Util.codeInsufficientData:
                                case Util.codeAuthenticationError:
                                default:
                                    Util.Message(userResponse.getMessage());
                                    showToast();
                                    break;
                            }
                        }else{
                            Util.Message("Entrar al error del server");
                            EventErrorServer eventErrorServer = new EventErrorServer(response.getReason());
                            EventBus.getDefault().post(eventErrorServer);
                        }
                    }
                    @Override
                    public void failure(RetrofitError error) {
                        Util.Message(error.getMessage());
                        //Util.Message(error.getResponse().getReason());
                        EventErrorServer eventErrorServer = new EventErrorServer(error.getMessage());
                        EventBus.getDefault().post(eventErrorServer);
                    }
                });
            }else{
                showToast();
            }
        }
        //Function of Toast
        public void showToast(){
            Toast toast = new Toast(getActivity().getApplicationContext());
            View toastCustomView = inflater.inflate(R.layout.custom_layout_toast,(ViewGroup)view.findViewById(R.id.llCustomToast));
            toast.setView(toastCustomView);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Subscribe
    public void onEvent(EventRestorePassword eventRestorePassword){
        restorePasswordFragment.setEventRestorePassword(eventRestorePassword);
        getActivity().getSupportFragmentManager().beginTransaction()
                .hide(getActivity().getSupportFragmentManager().findFragmentByTag(LoginFragment.fragmentForgotPassword))
                .add(R.id.fgMain, restorePasswordFragment,LoginFragment.fragmentRestorePassword)
                .addToBackStack(null)
                .commit();
    }
}
