package costumerapp.jaguarlabs.com.thecustomerapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.greenrobot.eventbus.EventBus;

import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventErrorServer;
import costumerapp.jaguarlabs.com.thecustomerapp.Event.EventRestorePassword;
import costumerapp.jaguarlabs.com.thecustomerapp.Interface.ServicesWeb;
import costumerapp.jaguarlabs.com.thecustomerapp.MainActivity;
import costumerapp.jaguarlabs.com.thecustomerapp.R;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.BaseResponse;
import costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model.RestorePasswordRequest;
import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Arturo on 06/01/2016.
 */
public class ForgotPasswordFragment extends Fragment {
    ImageView ivClearEmail;
    View view;
    EditText etEmailRestore;
    final static String request = "recuperar_password";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Create View of Forgot Password
        view = inflater.inflate(R.layout.forgot_password,container,false);
        etEmailRestore = (EditText) view.findViewById(R.id.etEmailRestorePassword);
        ivClearEmail = (ImageView) view.findViewById(R.id.ivCleanEmail);
        ivClearEmail.setVisibility(View.INVISIBLE);
        //Fuction of the clear Form
        /*ivClearEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etEmailRestore.setText("");
            }
        });*/
        ivClearEmail.setOnClickListener(new ClearText() {
            @Override
            public void onClick(View v) {
                memailTextView.setText("");
            }
        }.setEmailTextView(etEmailRestore));
        //Function of the enable o disable of button Clear text
        etEmailRestore.addTextChangedListener(etUsernameWatcher);
        TextView tvCancelRestore = (TextView) view.findViewById(R.id.tvCancelRestore);
        tvCancelRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.Message("Click en Cancel");
                ((RestorePasswordFragment.ListenerRestorePassword) getActivity()
                        .getSupportFragmentManager()
                        .findFragmentByTag(MainActivity.loginFragment)).listenerRestoreEvent(Util.canceled);
            }
        });
        String emailTextEditEmail = etEmailRestore.getText().toString();
        TextView tvSendRestore = (TextView) view.findViewById(R.id.tvSendRestore);
        Util.Message("asignado: "+emailTextEditEmail);
        tvSendRestore.setOnClickListener(new SendRestorePassword(inflater) {
            @Override
            public void onClick(View v) {
                Util.Message(etEmailRestore.getText().toString());
                Util.Message("Click en Enviar");
                super.onClick(v);
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ListenerDestroyForgotPassword)getActivity()
                .getSupportFragmentManager()
                .findFragmentByTag(MainActivity.loginFragment))
                .destroyForgotPassword(true);
    }

    //Function of enable of button of the clear Text Edit
    private final TextWatcher etUsernameWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            ivClearEmail.setVisibility(View.VISIBLE);
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length() == 0)
                ivClearEmail.setVisibility(View.INVISIBLE);
            else
                ivClearEmail.setVisibility(View.VISIBLE);
        }
    };
    //Enable Login
    /*@Override
    public void onDestroy() {
        ((MainActivity)getActivity()).ActiveLogin();
        super.onDestroy();

    }*/
    public abstract class SendRestorePassword implements View.OnClickListener{
        protected String email;
        private LayoutInflater inflater;

        public SendRestorePassword(LayoutInflater inflater){
            this.inflater = inflater;
        }
        @Override
        public void onClick(View v) {
            this.email = etEmailRestore.getText().toString();
            if(email.length() > 0){
                Util.Message("Llego aquí");
                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(Util.baseUrl)
                        .setClient(new OkClient(new OkHttpClient()))
                        .build();
                ServicesWeb servicesWeb = restAdapter.create(ServicesWeb.class);
                RestorePasswordRequest restorePasswordRequest = new RestorePasswordRequest(request,email);
                servicesWeb.getRestorePassword(restorePasswordRequest, new Callback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse baseResponse, Response response) {
                        EventErrorServer eventErrorServer;
                        if(response.getStatus()==200) {
                            Util.Message("Hay Respuesta exitosa");
                            switch (baseResponse.getCode()) {
                                case Util.codeRequestAfirm:
                                case Util.codeAuthenticationError:
                                    EventRestorePassword eventRestorePassword = new EventRestorePassword(email,Integer.valueOf(baseResponse.getCode()));
                                    EventBus.getDefault().post(eventRestorePassword);
                                    break;

                                case Util.codeBadRequest:
                                case Util.codeInsufficientData:
                                case Util.codeFailedToSendEmail:
                                    eventErrorServer = new EventErrorServer(baseResponse.getMessage());
                                    EventBus.getDefault().post(eventErrorServer);
                                    break;
                                default:
                                    eventErrorServer = new EventErrorServer(baseResponse.getMessage());
                                    EventBus.getDefault().post(eventErrorServer);
                                    break;
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Util.Message(error.getMessage());
                        //Util.Message(error.getResponse().getReason());
                        EventErrorServer eventErrorServer = new EventErrorServer(error.getMessage());
                        EventBus.getDefault().post(eventErrorServer);
                    }
                });
            }else{
                Util.Message("LLego al error");
                View layout = inflater.inflate(R.layout.custom_layout_toast_error_mail, (ViewGroup) view.findViewById(R.id.llCustomToast));
                Toast toast = new Toast(getActivity().getApplicationContext());
                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 250);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        }
    }

    public abstract class ClearText implements View.OnClickListener{
        protected TextView memailTextView;
        public ClearText setEmailTextView(TextView emailTextView){
            memailTextView = emailTextView;
            return this;
        }
    }
    public interface ListenerDestroyForgotPassword{
        void destroyForgotPassword(boolean b);
    }
}
