package costumerapp.jaguarlabs.com.thecustomerapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import costumerapp.jaguarlabs.com.thecustomerapp.R;

/**
 * Created by Arturo on 07/01/2016.
 */
public class SplashScreenFragment extends Fragment {
    //Create View for Splas Screem
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_layout,container,false);
        return view;
    }
}
