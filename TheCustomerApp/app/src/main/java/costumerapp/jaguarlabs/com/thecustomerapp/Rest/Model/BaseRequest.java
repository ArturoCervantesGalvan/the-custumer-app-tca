package costumerapp.jaguarlabs.com.thecustomerapp.Rest.Model;

import com.google.gson.annotations.SerializedName;

import costumerapp.jaguarlabs.com.thecustomerapp.Utility.Util;

/**
 * Created by Arturo on 10/02/2016.
 */
public class BaseRequest {
    @SerializedName(Util.request)
    public String request;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
